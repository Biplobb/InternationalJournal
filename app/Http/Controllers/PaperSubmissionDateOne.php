<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PaperSubmissionDateOne extends Controller
{
    public function submitPaper(){
        $date_info = DB::table('paper_submission_date_one')->get();
        return view('frontEnd.submit-paper')->with('date_info', $date_info);
    }
    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.paper-date-one');
    }
    public function store(Request $request){
        $data = array();
        $data['submition_date_3'] = $request->submition_date_3;
        $data['submition_date_1'] = $request->submition_date_1;
        $data['submition_date_2'] = $request->submition_date_2;

        DB::table('paper_submission_date_one')->insert($data);
        Session::put('message', 'Paper Submission date-1 Save Successfully!!');
        return Redirect::to('/add-date-1');
    }
    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $date_info = DB::table('paper_submission_date_one')->get();
        return view('admin.manage-date-submit')->with('date_info', $date_info);
    }
    public function edit($id){
        $date_info = DB::table('paper_submission_date_one')
            ->where('id', $id)
            ->first();

        return view('admin.edit-date-1')->with('date_info', $date_info);
    }
    public function update(Request $request){
        $data = array();
        $id = $request->id;
        $data['submition_date_3'] = $request->submition_date_3;
        $data['submition_date_1'] = $request->submition_date_1;
        $data['submition_date_2'] = $request->submition_date_2;

        DB::table('paper_submission_date_one')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-date-1');
    }
    public function delete($id){
        DB::table('paper_submission_date_one')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-date-1');
    }
}

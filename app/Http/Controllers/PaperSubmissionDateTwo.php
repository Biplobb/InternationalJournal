<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PaperSubmissionDateTwo extends Controller
{
    public function submitPaper(){
        $date_info = DB::table('paper_submission_date_two')->get();
        return view('submit-paper')->with('date_info', $date_info);
    }

    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.paper-date-two');
    }
    public function store(Request $request){
        $data = array();
        $data['submition_date_2'] = $request->submition_date_2;

        DB::table('paper_submission_date_two')->insert($data);
        Session::put('message', 'Paper Submission date-2 Save Successfully!!');
        return Redirect::to('/add-date-2');
    }
    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $date_info = DB::table('paper_submission_date_two')->get();
        return view('admin.manage-date-submit-two')->with('date_info', $date_info);
    }
    public function edit($id){
        $date_info = DB::table('paper_submission_date_two')
            ->where('id', $id)
            ->first();

        return view('admin.edit-date-2')->with('date_info', $date_info);
    }
    public function update(Request $request){
        $data = array();
        $id = $request->id;
        $data['submition_date_2'] = $request->submition_date_2;

        DB::table('paper_submission_date_two')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-date-2');
    }
    public function delete($id){
        DB::table('paper_submission_date_two')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-date-2');
    }
}

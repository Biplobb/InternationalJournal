<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
use PDF;

class CurrentIssueController extends Controller
{
    public function frontEndindex(){

        $issue_info=DB::table('tbl_current_issue_date')->select('tbl_current_issue_date.date as date','tbl_current_issue.month as month','tbl_current_issue.id as id','tbl_current_issue.title as title','tbl_current_issue.author as author','tbl_current_issue.details as details','tbl_current_issue.issue_image as issue_image','tbl_current_issue_date.volume as volume')->
        leftJoin('tbl_current_issue','tbl_current_issue_date.date','tbl_current_issue.month')
            ->get();


       /* $dogs = Dogs::orderBy('id', 'desc')->take(5)->get();*/
        $issue_date_info = DB::table('tbl_current_issue_date')->get();


        return view('frontEnd.current-issue')
        ->with('issue_date_info', $issue_date_info)
        ->with('issue_info', $issue_info);
    }

    public function issueDetails($id){
        $issue_info = DB::table('tbl_current_issue')->
            select('*')->where('id', $id)
            ->first();

        $issue_date_info=DB::table('tbl_current_issue')->select('tbl_current_issue_date.volume as volume')->
        leftJoin('tbl_current_issue_date','tbl_current_issue.month','tbl_current_issue_date.date')->
            where('tbl_current_issue.id',"=",$issue_info->id)->get();
        //$issue_date_info = DB::table('tbl_current_issue_date')->get();
        return view('frontEnd.issue_details',compact('issue_date_info','issue_info'));
    }

    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.current-issue');
    }
    public function store(Request $request){

        $data = array();
        $data['title'] = $request->title;
        $data['author'] = $request->author;
        $data['details'] = $request->details;
        $data['created_at'] =date("Y-m-d H:i:s A");
        $data['month'] =date('M Y');
        $data['volumes'] =$request->volumes;
        $data['unique'] =$request->unique;

        $image = $request->file('issue_image');

        if ($image) {
            
            $ext=$image->getClientOriginalName();
           $image_full_name = $ext;
            $upload_path = 'issue_image/';
          $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                $data['issue_image'] = $image_url;
                DB::table('tbl_current_issue')->insert($data);
                Session::put('message', 'Current Issue Add Successfully !');
                return Redirect::to('add-current-issue');
            }
            /*else {
                DB::table('tbl_current_issue')->insert($data);
                Session::put('message', 'Current Issue Add Successfully !');
                return Redirect::to('add-current-issue');
            }*/


        }


        DB::table('tbl_current_issue')->insert($data);
        Session::put('message', 'Current Issue Add Successfully!!');
        return Redirect::to('/add-current-issue');
    }
    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }

        $issue_info = DB::table('tbl_current_issue')->get();
        return view('admin.manage_current_issue')->with('issue_info', $issue_info);
    }
    public function edit($id){
        $issue_info = DB::table('tbl_current_issue')
            ->where('id', $id)
            ->first();

        return view('admin.edit_current_issue')->with('issue_info', $issue_info);
    }

    public function update(Request $request){
      
        $data = array();
        $id = $request->id;
        $data['title'] = $request->title;
        $data['author'] = $request->author;
        $data['details'] = $request->details;
        $data['volumes'] = $request->volumes;
        $data['unique'] = $request->unique;
        $image = $request->file('issue_image');

          if ($image) {
            
            $ext=$image->getClientOriginalName();
           $image_full_name = $ext;
            $upload_path = 'issue_image/';
          $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
                  
               $data['issue_image'] = $image_url;
              DB::table('tbl_current_issue')->where('id', $id)->update($data);
                Session::put('message', 'Current Issue update Successfully !');
                return Redirect::to('/manage-current-issue');
            }
            


        }
 
          DB::table('tbl_current_issue')->where('id', $id)->update($data);
                Session::put('message', 'Current Issue update Successfully !');
                return Redirect::to('/manage-current-issue');

       
        
        
      
    }
    public function delete($id){
        DB::table('tbl_current_issue')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-current-issue');
    }

    public function currentIndex(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.current-issue-date');
    }

    public function currentStore(Request $request){
        $data = array();
        $data['date'] = date('M Y');
        $data['volume'] = $request->volume;

        DB::table('tbl_current_issue_date')->insert($data);
        Session::put('message', 'Current Issue Date Add Successfully!!');
        return Redirect::to('/add-current-issue-date');
    }
    public function currentManage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $issue_info = DB::table('tbl_current_issue_date')->get();
        return view('admin.manage_current_issue_date')->with('issue_info', $issue_info);
    }
    public function currentEdit($id){
        $issue_info = DB::table('tbl_current_issue_date')
            ->where('id', $id)
            ->first();

        return view('admin.edit_current_issue_date')->with('issue_info', $issue_info);
    }
    public function currentUpdate(Request $request){
        $data = array();
        $id = $request->id;
        $data['date'] = $request->date;
        $data['volume'] = $request->volume;


        DB::table('tbl_current_issue_date')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-current-issue-date');
    }
    public function currentDelete($id){
        DB::table('tbl_current_issue_date')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-current-issue-date');
    }

    public function pdf($id){
        $data = DB::table('tbl_current_issue')
            ->where('id', $id)
            ->first();
    }
}

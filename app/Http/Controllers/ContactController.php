<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;
use Mail;

class ContactController extends Controller
{
    public function index(){

        return view('frontEnd.contact');
    }
    public function store(Request $request){
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['details'] = $request->details;

      $success =   DB::table('tbl_contact')->insert($data);
     
     if($success){
         mail("info@ijcrs.org",$data['email'],$data['details']);
        };
            

        Session::put('message', 'Your Message Sent Successfully!!');
        return Redirect::to('/contact');
    }
    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $contact_info = DB::table('tbl_contact')->get();
        return view('admin.manage_contact')->with('contact_info', $contact_info);
    }

    public function delete($id){
        DB::table('tbl_contact')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-contact');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class CallPaperController extends Controller
{
    public function frontEndindex(){
        $paper_info = DB::table('call_for_paper')->get();
        return view('frontEnd.call-for-paper')->with('paper_info', $paper_info);;
    }

    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.call-paper');
    }
    public function store(Request $request){
        $data = array();
        $data['call_for_paper_1'] = $request->call_for_paper_1;
        $data['call_for_paper_2'] = $request->call_for_paper_2;
        $data['call_for_paper_3'] = $request->call_for_paper_3;

        DB::table('call_for_paper')->insert($data);
        Session::put('message', 'Call For Paper date Save Successfully!!');
        return Redirect::to('/add-call-paper');
    }
    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $paper_info = DB::table('call_for_paper')->get();
        return view('admin.manage_call_for_paper')->with('paper_info', $paper_info);
    }

    public function edit($id){
        $paper_info = DB::table('call_for_paper')
            ->where('id', $id)
            ->first();

        return view('admin.edit_call_for_paper')->with('paper_info', $paper_info);
    }

    public function update(Request $request){
        $data = array();
        $id = $request->id;
        $data['call_for_paper_1'] = $request->call_for_paper_1;
        $data['call_for_paper_2'] = $request->call_for_paper_2;
        $data['call_for_paper_3'] = $request->call_for_paper_3;

        DB::table('call_for_paper')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-call-paper');
    }

    public function delete($id){
        DB::table('call_for_paper')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-call-paper');
    }
}

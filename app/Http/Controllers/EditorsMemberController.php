<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class EditorsMemberController extends Controller
{
   public function frontEndindex(){
       $paper_info = DB::table('editors_member')->get();
       return view('frontEnd.editors-member')->with('paper_info', $paper_info);
   }
    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.editors-member');
    }
    public function store(Request $request){
        $data = array();
        $data['name'] = $request->name;
        $data['department'] = $request->department;


        DB::table('editors_member')->insert($data);
        Session::put('message', 'Editors  Member  Add Successfully!!');
        return Redirect::to('/add-editors-member');
    }

    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $paper_info = DB::table('editors_member')->get();
        return view('admin.manage_editors_member')->with('paper_info', $paper_info);
    }

    public function edit($id){
        $paper_info = DB::table('editors_member')
            ->where('id', $id)
            ->first();

        return view('admin.edit_editors_member')->with('paper_info', $paper_info);
    }
    public function update(Request $request){
        $data = array();
        $id = $request->id;
        $data['name'] = $request->name;
        $data['department'] = $request->department;


        DB::table('editors_member')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-editors-member');
    }
    public function delete($id){
        DB::table('editors_member')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-editors-member');
    }
}

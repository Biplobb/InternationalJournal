<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function search(Request $request)

    {
       
         $results = array();
        if ($request->title && $request->searchField == 'authors') {
            $results = DB::table('tbl_current_issue')->select('id', 'title','issue_image' ,'author', 'details', 'volumes')
                ->where('author', 'like', '%' . $request->title . '%')
                ->get();
            $result = DB::table('tbl_current_issue')->select('id', 'title', 'author', 'details', 'volumes','issue_image' )
                ->where('author', 'like', '%' . $request->title . '%')->take(1)
                ->first();

            return view('frontEnd.searching', compact('results','result'));
        }

        if ($request->title && $request->searchField == 'title') {
            $results = DB::table('tbl_current_issue')->select('id', 'title', 'author', 'details', 'volumes','issue_image' )
                ->where('title', 'like', '%' . $request->title . '%')
                ->get();

            $result = DB::table('tbl_current_issue')->select('id', 'title', 'author', 'details', 'volumes','issue_image' )
                ->where('title', 'like', '%' . $request->title . '%')->take(1)
                ->first();


            return view('frontEnd.searching', compact('results','result'));
        }

        if (empty($request->title)) {


            return view('frontEnd.searchpdf');
        }

        if ($request->title) {
            $results = DB::table('tbl_current_issue')->select('id', 'title', 'author', 'details', 'volumes','issue_image' )
                ->where('title', 'like', '%' . $request->title . '%')
                ->orWhere('author', 'like', '%' . $request->title . '%')
                ->get();

                $result = DB::table('tbl_current_issue')->select('id', 'title', 'author', 'details', 'volumes','issue_image' )
                    ->where('title', 'like', '%' . $request->title . '%')
                    ->orwhere('author', 'like', '%' . $request->title . '%')
                    ->take(1)
                    ->first();
                return view('frontEnd.searching', compact('results','result'));



        }




        }







    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

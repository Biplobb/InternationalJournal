<?php

namespace App\Http\Controllers;

use App\Paper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PaperSubmitController extends Controller
{
    public function frontEndIndex(){
        $paper_info = DB::table('tbl-paper-submit')->get();
        $date_info = DB::table('paper_submission_date_one')->get();
        $notice_info = DB::table('notice')->get();

        return view('index')
            ->with('date_info', $date_info)
            ->with('notice_info', $notice_info)
            ->with('paper_info', $paper_info);
    }

    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.paper-submit');
    }

    public function store(Request $request){
        $data = array();
        $data['submition_date'] = $request->submition_date;

        DB::table('tbl-paper-submit')->insert($data);
        Session::put('message', 'Paper Submission date Save Successfully!!');
        return Redirect::to('/add-paper');
    }

    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $paper_info = DB::table('tbl-paper-submit')->get();
        return view('admin.manage-paper-submit')->with('paper_info', $paper_info);
    }



    public function edit($id){
        $paper_info = DB::table('tbl-paper-submit')
            ->where('id', $id)
            ->first();

        return view('admin.edit-paper-submit')->with('paper_info', $paper_info);
    }

    public function update(Request $request){
        $data = array();
        $id = $request->id;
        $data['submition_date'] = $request->submition_date;

        DB::table('tbl-paper-submit')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-paper');
    }

    public function delete($id){
        DB::table('tbl-paper-submit')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-paper');
    }
}

<?php

namespace App\Http\Controllers;
use DB;
use PDF;
use Illuminate\Http\Request;

class PdfController extends Controller
{

//        $pdf=\Barryvdh\DomPDF\PDF::loadview('frontEnd.invoice');

        public function getpdf(Request $request)
    {
        $tbl_current_issue= DB::table("tbl_current_issue")->where('id',$request->id)->get();
        view()->share('tbl_current_issue',$tbl_current_issue);

            $pdf = PDF::loadView('frontEnd.invoice');
            return $pdf->download('invoice.pdf');



    }



}

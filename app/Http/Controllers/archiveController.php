<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use PDF;


class archiveController extends Controller
{

    public function index(){

          $archivess = DB::table('tbl_current_issue')
            ->select('created_at','unique')
            ->groupBy('unique')
            ->get();
        $a=sizeof($archivess)-1;
        $archives = DB::table('tbl_current_issue')
            ->select('created_at','month','unique','volumes')->orderBy('unique', 'asc')->take($a)
            ->groupBy('unique')
            ->get();
       
        return view('frontEnd.archive',compact('archives','issue_date_info'));
    }

    public function getData(Request $request)
    {



         $data=DB::table('tbl_current_issue')
            ->select('title','volumes','author','id','created_at','issue_image')
            ->where('month','=',$request->month)
            ->get();






        return view('frontEnd.arcivess',compact('data'));


       /* $option='';
        $data=DB::table('tbl_current_issue')
            ->select('title','author','id','created_at','issue_image')
            ->where('month','=',$request->month)
            ->get();




        foreach($data as $d){
            $option.="<table border='10'>
<thead>
<!--<tr>
<th>Title</th>
<th>Author</th>
 
</tr>-->
</thead>
             
                <tbody>
                 <tr>
                 
                  <h4>TITLE:.$d->title.</h4>
                <h4>AUTHOR:.$d->author.</h4>
                
                <h2> <a href='$d->issue_image' download>Download</a><br/><br/><br/></h2>
</tr>
               
               
                 

                </tr>
                </tbody>
                
                </table>";


        }
        return $option;*/
    }

    public function getfile($id){

        $tbl_current_issue=DB::table('tbl_current_issue')
            ->select('title','author','details')
            ->where('id','=',$id)
            ->get();

        view()->share('tbl_current_issue',$tbl_current_issue);

        $pdf = PDF::loadView('frontEnd.invoice');
        return $pdf->download('invoice.pdf');


    }
}

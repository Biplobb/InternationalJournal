<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Redirect;

class NoticeController extends Controller
{
    public function index(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.notice');
    }
    public function store(Request $request){
        $data = array();
        $data['notice'] = $request->notice;

        DB::table('notice')->insert($data);
        Session::put('message', 'Notice Add Successfully!!');
        return Redirect::to('/add-notice');
    }
    public function manage(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        $notice_info = DB::table('notice')->get();
        return view('admin.manage-notice')->with('notice_info', $notice_info);
    }
    public function delete($id){
        DB::table('notice')
            ->where('id', $id)
            ->delete();
        return Redirect::to('/manage-notice');
    }

    public function edit($id){
        $notice_info = DB::table('notice')
            ->where('id', $id)
            ->first();

        return view('admin.edit-notice')->with('notice_info', $notice_info);
    }
    public function update(Request $request){
        $data = array();
        $id = $request->id;
        $data['notice'] = $request->notice;

        DB::table('notice')
            ->where('id', $id)
            ->update($data);

        return Redirect::to('/manage-notice');
    }
}

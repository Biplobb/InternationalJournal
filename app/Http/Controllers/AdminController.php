<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;

class AdminController extends Controller
{
   public function index(){
       return view('admin.admin_login');
   }

   public function AdminIndex(){
       $id = Session::get('id');
       if ($id == NULL) {
           return Redirect::to('/admin-panel')->send();
       }
       return view('admin.index');
   }
   public function auth_check(Request $request){
       $email = $request->email;
       $password = $request->password;

       $result = DB::table('tbl_admin_login')
           ->where('email', $email)
           ->where('password', $password)
           ->first();
       if($result){
           Session::put('id',$result->id);
           return Redirect::to('/admin');
       }else{
           Session::put('exception','Email or Password Invalid');
           return Redirect::to('admin-panel');
       }

   }


    public function logout() {
        Session::put('id', null);
        Session::put('message', 'You are successfully logout!!');
        return Redirect::to('/admin-panel');
    }

}

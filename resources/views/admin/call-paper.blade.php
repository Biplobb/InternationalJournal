@include('admin.layouts.head')
<body class="theme-red">


<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">IJCRS - ADMIN PANEL</a>
        </div>

    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">


        @include('admin.layouts.menu')


    </aside>

</section>

<section class="content">
    <div class="container-fluid">



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Call For Paper
                    </h2>
                    <h3 style="color: green">
                        <?php
                        $message = Session::get('message');
                        if ($message) {
                            echo $message;
                            Session::put('message', null);
                        }
                        ?>
                    </h3>

                </div>
                <div class="body">
                    {!! Form::open(['url' => '/save-call-paper','method'=>'post']) !!}



                    <label for="email_address">Add Call For Paper</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="call_for_paper_1" class="form-control" placeholder="Enter your Call For Paper Date One">
                        </div>

                        <div class="form-line">
                            <input type="text" name="call_for_paper_2" class="form-control" placeholder="Enter your Call For Paper Date Two">
                        </div>

                        <div class="form-line">
                            <input type="text" name="call_for_paper_3" class="form-control" placeholder="Enter your Call For Paper Date Three">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>



    </div>
</section>

@include('admin.layouts.footer')
@include('admin.layouts.head')
<body class="theme-red">


<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">IJCRS - ADMIN PANEL</a>
        </div>

    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">


        @include('admin.layouts.menu')



    </aside>

</section>

<section class="content">
    <div class="container-fluid">



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Manage Call For Paper
                    </h2>

                </div>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>

                            <th>Call For Paper</th>
                            <th>Edit/Delete</th>

                        </tr>
                        </thead>
                        <tbody>


                        @foreach($paper_info as $paper)

                            <tr>
                                <td>{{ $paper->call_for_paper_1 }}</td>
                                <td>
                                    <a class="btn btn-info" href="{{URL::to('/edit-call-paper/'.$paper->id)}}">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a class="btn btn-danger" href="{{URL::to('/delete-call-paper/'.$paper->id)}}" >
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>

                            </tr>

                            <tr>
                                <td>{{ $paper->call_for_paper_2 }}</td>
                            </tr>

                            <tr>
                                <td>{{ $paper->call_for_paper_3 }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</section>

@include('admin.layouts.footer')
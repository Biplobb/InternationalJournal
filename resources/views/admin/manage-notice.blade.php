@include('admin.layouts.head')
<body class="theme-red">


<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">IJCRS - ADMIN PANEL</a>
        </div>

    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">


        @include('admin.layouts.menu')




    </aside>

</section>

<section class="content">
    <div class="container-fluid">



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Manage Notice
                    </h2>

                </div>
                <div class="body table-responsive">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                        <tr>

                            <th>Notice</th>
                            <th>Edit/Delete</th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($notice_info as $notice)
                        <tr>


                                <td>{{ $notice->notice }} </td>
                                <td>
                                    <a class="btn btn-info" href="{{URL::to('/edit-notice/'.$notice->id)}}">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a class="btn btn-danger" href="{{URL::to('/delete-notice/'.$notice->id)}}" >
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



    </div>
</section>

@include('admin.layouts.footer')
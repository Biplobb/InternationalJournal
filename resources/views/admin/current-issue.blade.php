<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Editors | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('back-end/')}}/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('back-end/')}}/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('back-end/')}}/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('back-end/')}}/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('back-end/')}}/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->

<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">IJCRS - ADMIN PANEL</a>
        </div>

    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">


        @include('admin.layouts.menu')



    </aside>

</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Current Issue</h2>
        </div>

        <!-- CKEditor -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Add Current Issue</h2>
                        <h3 style="color: green">
                            <?php
                            $message = Session::get('message');
                            if ($message) {
                                echo $message;
                                Session::put('message', null);
                            }
                            ?>
                        </h3>

                    </div>


                    <div class="body">
                        {!! Form::open(['url' => '/save-current-issue','method'=>'post','enctype' => 'multipart/form-data']) !!}


                        <div class="form-group">

                            <div class="form-line">
                                <input type="text" name="volumes" class="form-control" placeholder="Enter volumes">
                            </div><br/>
                            <div class="form-line">
                                <input type="text" name="unique" class="form-control" placeholder="Every month same number">
                            </div><br/>
 <div class="form-line">
                                <input type="text" name="title" class="form-control" placeholder="Enter Title">
                            </div><br/>

                            <div class="form-line">
                                <input type="text" name="author" class="form-control" placeholder="Enter Author Name">
                            </div><br/>


                           <strong>Enter  Details:</strong><br/>

                            <textarea id="ckeditor"  name="details" class="form-control" ></textarea>

                            <div class="form-line">
                                <input type="file" name="issue_image" class="form-control">
                            </div><br/>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                            {!! Form::close() !!}
                        </div>


                    </div>
                </div>
            </div>

        </div>
</section>

<!-- Jquery Core Js -->
<script src="{{asset('back-end/')}}/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="{{asset('back-end/')}}/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="{{asset('back-end/')}}/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{asset('back-end/')}}/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('back-end/')}}/plugins/node-waves/waves.js"></script>

<!-- Ckeditor -->
<script src="{{asset('back-end/')}}/plugins/ckeditor/ckeditor.js"></script>

<!-- TinyMCE -->
<script src="{{asset('back-end/')}}/plugins/tinymce/tinymce.js"></script>

<!-- Custom Js -->
<script src="{{asset('back-end/')}}/js/admin.js"></script>
<script src="{{asset('back-end/')}}/js/pages/forms/editors.js"></script>

<!-- Demo Js -->
<script src="{{asset('back-end/')}}/js/demo.js"></script>
</body>

</html>
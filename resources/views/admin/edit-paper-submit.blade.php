@include('admin.layouts.head')
<body class="theme-red">


<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="#">IJCRS - ADMIN PANEL</a>
        </div>

    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">


        @include('admin.layouts.menu')


    </aside>

</section>

<section class="content">
    <div class="container-fluid">



        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                       Update Paper Submission
                    </h2>
                    <h3 style="color: green">
                        <?php
                        $message = Session::get('message');
                        if ($message) {
                            echo $message;
                            Session::put('message', null);
                        }
                        ?>
                    </h3>

                </div>
                <div class="body">
                    {!! Form::open(['url' => '/update-paper','method'=>'post']) !!}



                    <label for="email_address">Submission</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="submition_date" class="form-control" value="{{ $paper_info->submition_date }}">
                            <input type="hidden" name="id" value="<?php echo $paper_info->id;?>">
                        </div>
                    </div>


                    <button type="submit" class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>



    </div>
</section>

@include('admin.layouts.footer')
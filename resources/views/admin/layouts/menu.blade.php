<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
            <a href="{{URL::to('/admin')}}">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/add-paper')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Paper-Submit</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-paper')}}">
                <i class="material-icons">view_list</i>
                <span>Manage-Paper-Submit</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/add-date-1')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Important Date-1</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-date-1')}}">
                <i class="material-icons">view_list</i>
                <span>Manage-Important-Date-1</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/add-call-paper')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Call For Paper</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-call-paper')}}">
                <i class="material-icons">view_list</i>
                <span>Manage Call For Paper</span>
            </a>
        </li>

        <li>
            <a href="{{URL::to('/add-editors-member')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Editors Member</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-editors-member')}}">
                <i class="material-icons">view_list</i>
                <span>Manage Editors Member</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/add-notice')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Notice</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-notice')}}">
                <i class="material-icons">view_list</i>
                <span>Manage Notice</span>
            </a>
        </li>

        <li>
            <a href="{{URL::to('/add-current-issue')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Current Issue</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-current-issue')}}">
                <i class="material-icons">view_list</i>
                <span>Manage Current Issue</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/add-current-issue-date')}}">
                <i class="material-icons">text_fields</i>
                <span>Add Current Issue Date</span>
            </a>
        </li>
        <li>
            <a href="{{URL::to('/manage-current-issue-date')}}">
                <i class="material-icons">view_list</i>
                <span>Manage Current Issue Date</span>
            </a>
        </li>

        <li>
            <a href="{{URL::to('/manage-contact')}}">
                <i class="material-icons">view_list</i>
                <span>Manage Contact</span>
            </a>
        </li>



    </ul>
</div>
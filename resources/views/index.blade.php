@include('frontEnd.layouts.head')

<div class="fix main">

   @include('frontEnd.layouts.header-top')


    <div class="fix slider_area">
        <div class="fix slider">
            <div class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
                    <a href="#"><img src="{{ asset('front-end/ijirk/img/1.jpg') }}" data-thumb="images/1.jpg" alt="" title="IJCRS aims at the creation, dissemination and re-invention of knowledge by the extraordinary and innovative research works" /></a>
                    <a href="#"><img src="{{ asset('front-end/ijirk/img/2.jpg') }}" data-thumb="images/2.jpg" alt="" title="IJCRS gives open access to every last bit of it substance for the worldwide trade of information" /></a>
                    <a href="#"><img src="{{ asset('front-end/ijirk/img/3.jpg') }}" data-thumb="images/3.jpg" alt="" title="IJCRS is dedicated to serve diverse global communities by an unparalleled commitment to quality, reliability, and creative and research works" /></a>
                    <a href="#"><img src="{{ asset('front-end/ijirk/img/4.jpg') }}" data-thumb="images/4.jpg" alt="" title="IJCRS engages its noble efforts for the development and tries its best to give you better" /></a>
                    <a href="#"><img src="{{ asset('front-end/ijirk/img/5.jpg') }}" data-thumb="images/slider_eco.jpg" alt="" title="IJCRS is much caring, easy and faster way to publish your article without hesitation" /></a>
                </div>
            </div>
        </div>
    </div>

    <div class="fix maincontent">

      @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="content fix">
                @foreach($date_info as $date)

                <a href="{{ URL::to('submit-paper') }}"><p><u><span class="submit_paper_blink">{{ $date->submition_date_3 }}</span></u></p></a>

                    @endforeach


            </div>
            <div class="call_paper_well_submit_area fix">

                <div class="well_come fix">
                    <p>
                        <span class="welcome_to_ijirk_blink">WELCOME TO IJCRS!</span>
                    </p>
                </div>

            </div>
            <div class="ijirk_text fix">
                <p><strong>International Journal of Creative Research and Studies (IJCRS)</strong> is a high quality open-access, peer-reviewed and refereed multidisciplinary research journal, dedicated to serve the society by the global dissemination of information through an unparalleled commitment to quality, reliability, and innovation and research work which is published by the Knowledge Ridge Publishers. International Journal of Creative Research and Studies (IJCRS) is a brisk educational research platform providing those individuals an ideal feeling to accomplish their desires who long for a refined betterment in their respective arenas. <a href="{{ URL::to('welcome_note') }}"><span class="continue_reading">Continue Reading >></span></a></p>
            </div>
            <div class="notice_board_area fix">
                <div class="notice_board_title fix">
                    <p>Notice Board</p>
                </div>
                <div class="notice_board_description fix">

                    @foreach($notice_info as $notice)
                    <p><?php echo $notice->notice ?>	<img src="{{ asset('front-end/ijirk/images/blinking.gif') }}" alt="" /></p><br>

                    @endforeach

                </div>
            </div>
            <div class="why_ijirk_area fix">
                <div class="why_ijirk_title fix">
                    <p>WHY IJCRS...???</p>
                </div>
                <div class="why_ijirk_description fix">
                    <p>•  Best Global Knowledge Sharing Platform</p>
                    <p>•  Originality and Innovation</p>
                    <p>•  International Quality and Standards</p>
                    <p>•  Encouraging Quality Research</p>
                    <p>•  360 Degrees Coverage Of Research Field</p>
                    <p>•  Rapid Review Process</p>
                    <p>•  Rapid Publication Process</p>
                    <p>•  High quality Open access Journal</p>
                    <p>•  Effective Editorial Standards</p>
                    <p>•  Both Online and Print Version</p>
                    <p>•  Rigorous Peer Review Process</p>
                    <p>•  Strong National and International network & Collaboration</p>
                    <p>•  Broad visibility through Promotion and Distribution</p>
                    <p>•  Papers abstract / indexed by all the major scientific indexing services</p>
                    <p>•  Reliable, Easy and Rapidly growing Publication with nominal publication fees</p>
                    <p>•  High level of commitment</p>

                </div>
            </div>
        </div>

        @include('frontEnd.layouts.down-footer')


    </div>

</div>
<script type="text/javascript" src="{{ asset('front-end/ijirk/js/jquery-1.9.0.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('front-end/ijirk/js/jquery.nivo.slider.pack.js') }}"></script>

<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
</script>



</body>


</html>
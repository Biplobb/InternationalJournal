@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="contact_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">CONTACT US</h1><br/>


                <p>For any kind of feedback, suggestions and query feel free to email us to <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>info@ijcrs.org</u></strong></a><span>.Your query will be resolved within 24 working hours. Your feedback and suggestions will enrich our journal.  </p><br><br>
                <p> <strong>N.B.:</strong>  Please check out the <a href="{{URL::to('faq')}}"><strong> FAQ </strong></a>section before sending any query. It may solve your query immediately.</p><br><br>
                <div class="">
                    <div class="panel panel-primary">

                        <div class="panel-heading">Contact Us</div>
                        <div class="panel-body">

                            <form data-toggle="validator" role="form" action="{{URL::to('save-contact')}}" method="post">

                                {{csrf_field()}}

                                <div class="form-group">
                                    <label class="control-label" for="inputName">Name</label>
                                    <input class="form-control" name="name" data-error="Please enter name field." id="inputName" type="text" required />
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail" class="control-label">Email</label>
                                    <input type="email" name="email" class="form-control" id="inputEmail"  required>
                                    <div class="help-block with-errors"></div>
                                </div>



                                <div class="form-group">
                                    <label class="control-label" for="inputName">Details</label>
                                    <textarea class="form-control" name="details" data-error="Please enter Details field." id="inputName"  required=""></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">
                                        Submit
                                    </button>
                                </div>
                                <h3 style="color: green">
                                    <?php
                                    $message = Session::get('message');
                                    if ($message) {
                                        echo $message;
                                        Session::put('message', null);
                                    }
                                    ?>
                                </h3>
                            </form>

                        </div>
                    </div>
                </div>

                <br>
                <br>
                <br>
            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
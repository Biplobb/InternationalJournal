@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="submit_your_paper_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">Submit Your Paper</h1><br/>

                <p>We welcome big achievers, professors, research scholars to contribute their original works in forms of case studies, empirical studies, meta-analysis and theoretical articles and illuminate the pages with their universal ideas and fresh perspectives to make the journal synonymous to the entire research field.</p><br/>
                <p><strong>Important Dates</strong></p>

                @foreach($date_info as $date)

                    <p>– {{ $date->submition_date_1 }}</p>
                    <p>– {{ $date->submition_date_2 }}</p>

                @endforeach









                <br/>
                <h3>Paper Submission</h3><br/>
                <p><strong>Submission Preparation Checklist</strong></p><br/>
                <p>As part of the submission process, authors are required to check off their submission’s compliance with all of the following items, and submissions may be returned to authors that do not adhere to these guidelines.
                </p><br/>
                <p>1.	The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).
                </p><br/>
                <p>2.	The submission file is in Microsoft Word document file format</p><br/>
                <p>3.	Where available, URLs for the references have been provided.</p><br/>
                <p>4.	The text is single-spaced type A4 pages, 1.15 line spacing; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.
                </p><br/>
                <p>5.	The file should never exceed 25 pages/8000 words/8 MB owing to file restriction implied.</p><br/>
                <p>6.	The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines, which is found in About the Journal.</p><br/>
                <p>7.	If submitting to a peer-reviewed section of the journal, the instructions in Ensuring a Blind Review have been followed.</p><br/>
                <p><strong>Submit Your Paper</strong></p>
                <p>Submit your manuscript directly to <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>submission@ijcrs.com</u></strong></a></span></p>
                <br/>
                <p><strong>Publication Process</strong></p>
                <p>The publication process is consisting with the following steps. The ultimate objectives of this process are Quality, Quality and Quality. Full process will be completed expected within 5-7 days.</p>
                <br/>
                <p>1. After receiving the manuscript we will send a confirmation e-mail to the author.</p><br/>
                <p>2. Primary quality will be checked by the Editor.</p><br/>
                <p>3. The manuscript will be sent for double blinded review.</p><br/>
                <p>4. Based on the double blinded review editor will take decision and the decision will be communicated to the author. Decisions can be three types like accepted without revision, resubmit with major/minor revisions, rejected.</p><br/>
                <p>5. After the final review process if the paper will accepted we will send a payment request to the author. </p><br/>
                <p>6. Author need to deposit publication fee within 7 days after the payment request.</p><br/>
                <p>7. Payment will be confirmed.</p><br/>
                <p>8. 'Author Declaration Form' need to submit by the author.</p>
                <br/>
                <p><strong>Publication Fee</strong></p>
                <p>Our eminent editorial board members and reviewers are working voluntarily for the development of the research field. The publication fee is charged for the cost of managing the journal website and management team. Payment procedure will be notified to the author through e-mail after the successful review process.    </p><br/>
                <p>If the paper is accepted for publication, author(s) will be asked to pay 100 USD as article publication fee. Waiver policy is not applicable. Author(s) will be entitled to one copy of the printed journal with free of charge I E one printed copy is provided against one article disregarding the number of the authors. Authors can also get additional copies of the printed journal by paying 30 USD for each additional copy.</p>
                <br/>
            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>

</html>
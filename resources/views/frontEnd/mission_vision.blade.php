@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="mision_vision_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">MISSION & VISION</h1><br/>
               <h4 style="text-align: center">MISSION</h4> 
<p> The mission of the International Journal of Creative Research and Studies (IJCRS) is to provide the worldwide community with a platform for the latest research and advancements in different disciplines of Business, Economics & Management; Social Science, Literature, Arts & Humanities; Engineering & Technology; Life Science & Physical Science, Health & Medical Science. IJCRS focuses on interdisciplinary techniques and state-of-the-art research among various disciplines and to serve diverse global communities by advancing, disseminating and applying innovative knowledge for improving the quality of life.</p><br/>

 <p> The mission of IJCRS is to be the premier international scholarly academy for exchange and advancement of research. In order to accomplish the mission, the IJCRS promotes the exchange of ideas in research, teaching, and managerial practices through formal presentations of research, discussions, seminars, and publication of scholarly journals. </p> <br/>


 <h4 style="text-align: center">VISION</h4><p>
The vision of the academy is to promote international exchange of state-of-the-art knowledge and ideas in research, learning, and teaching to help improve nations' productivity and competitiveness. The vision of IJCRS is to provide an academic medium and an important reference for the advancement and dissemination of research results that support high-level learning, teaching and research in the fields of Business, Economics & Management; Social Science, Literature, Arts & Humanities; Engineering & Technology; Life Science & Physical Science, Health & Medical Science. With a Panoramic spectacle of research components, the IJCRS pronounces a strong note of consistency, stringency and a specialised inclination towards the subjects concerned.</p>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>
</html>
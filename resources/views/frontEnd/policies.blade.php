@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="policies_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">POLICIES</h1><br/>
                <h3>OPEN ACCESS POLICY</h3>
                <p>Open Access Initiative (OAI) is a new paradigm in publishing domain. Its objective is to pioneer and promote models that ensure free access to scholarly & research journals. International Journal of Creative Research and Studies (IJCRS) is an electronic gateway to global journal literature in open access domain. International Journal of International Journal of Creative Research and Studies (IJCRS) gives open access to every last bit of it substance on the rule that making research unreservedly accessible to people in general backings a more amazing worldwide trade of information. Such get to is connected with expanded readership and expanded citation of a writer's work. Thus, all articles published under open access can be accessed by anyone with internet connection without financial, legal or technical barriers. Those who wish to read and use it working in any institution across the globe, can read, down load, copy, distribute, print, search or link the full text of the papers or articles published in IJCRS without taking any prior permission either from the publisher or from the author. High visibility and maximum global distribution journal is more easily included and searchable in search engines and indexing databases.

                </p>
                <h3>PRIVACY POLICY</h3>
                <p>The names and email addresses entered in this journal site will be utilized solely for the expressed purposes of this journal and won't be made accessible to any other party for whatever possible reason or to any viable gathering.</p>

                <h3>PUBLICATION POLICY</h3>
                <p>
                    International Journal of Creative Research and Studies (IJCRS) publishes papers that are judged and reviewed by editorial team types of articles include original research papers, articles, reviews, mini-reviews, short research communications, synopsis and Case studies. International Journal of Creative Research and Studies (IJCRS) aims at rapid publication of quality research results and reviews while maintaining rigorous review process. The main criteria for accepting a manuscript for publication in IJCRS are originality, innovative and interest.
The authors should note that there is no submission fee; however, there is a reasonable publication fee of each accepted article to meet the cost of Website Management Charges, server and website update and maintenance, Manuscript handling, Review process, Typesetting, copy-editing, Language correction, tagging and indexing of articles, Printing, Electronic composition and production, hosting the final article on dedicated servers, electronic archiving, Office cum admin expenses and other recurring expenses.


                </p>


            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>
</html>
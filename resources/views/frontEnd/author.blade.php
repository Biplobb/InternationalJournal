@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="author_right_ijirk_text fix">
                <h2>AUTHOR’S RIGHTS AND OBLIGATIONS</h2><br/>
                <p><strong>All authors published their research papers in International Journal of Creative Research and Studies are entitled for following rights and obligations:</strong></p>
                <p>1. Authors hold full copyright and self-archiving rights, they transfer the publishing rights to IJCRS.
                </p><br/>
                <p>2. We do decline to publish material where a pre-print or working paper has been previously mounted online.

                </p><br/>
                <p>3.We allow author to get their seminar papers published with note about the seminar if the paper is not mounted online.
                </p><br/>
                <p>4.   The research and review papers published in IJCRS can be archived in any private of public archives online or offline. Authors are allowed to archive their article in open access repositories as post-prints. Note: a post-print is the version incorporating changes and modifications resulting from peer-review comments.
                </p><br/>
                <p>5.   Authors are free to use link to our published papers and share the published papers online or offline in the final format printed on the Journal website.
                </p><br/>
                <p>6.   Authors are free to use link to our published papers and share the published papers online or offline in the final format printed on the Journal website.
                </p><br/>
                <p>7.   Authors can index and store the published papers in the private or public archives or repositories like university database, Google Scholar, Academia, Mendeley, Scribd etc.
                </p><br/>
                <p>8.We promote sharing of knowledge with due credit to the authors and researchers of the papers published with IJCRS.
                </p>

                <br/><br/>
                <h3>COPYRIGHT STATEMENT</h3>
                <p><strong>All authors who submit their paper for publication will abide by following provisions of the copyright transfer:</strong>
                </p><br/>
                <p>1.   The copyright of the paper rests with the authors. And they are transferring the copyright to publish the article and used the article for indexing and storing for public use with due reference to published matter in the name of concerned authors.
                </p><br/>
                <p>2.	The authors reserve all proprietary rights such as patent rights and the right to use all or part of the article in future works of their own such as lectures, press releases, and reviews of textbooks.
                    .
                </p><br/>
                <p>3.	In the case of republication of the whole, part, or parts thereof, in periodicals or reprint publications by a third party, written permission must be obtained from the Executive Editor of IJCRS.
                </p><br/>
                <p>4.	The authors declare that the material being presented by them in this paper is their original work, and does not contain or include material taken from other copyrighted sources. Wherever such material has been included, it has been clearly indented or/and identified by quotation marks and due and proper acknowledgements given by citing the source at appropriate places.
                </p><br/>
                <p>5.	The paper, the final version of which they submit, is not substantially the same as any that they had already published elsewhere.
                </p><br/>
                <p>6.	They declare that they have not sent the paper or any paper substantially the same as the submitted one, for publication anywhere else.
                </p><br/>
                <p>7.	Furthermore, the author may only post his/her version provided acknowledgement is given to the original source of publication in this journal and a link is inserted wherever published.
                </p><br/>
                <p>8.	All contents, Parts, written matters, publications are under copyright act taken by IJCRS.
                </p><br/>
                <p>9.	After publication If found any copyright content or any issue related to copyright in future than IJCRS have rights to withdraw the paper without prior notice to authors.
                </p><br/>
                <p>10.	Published articles will be available for use by scholars and researchers.
                </p><br/>
                <p>11.	IJCRS and the Journal Editors/Reviewers will not be held liable against all copyright claims of publishing authors or any third party in lawsuits that may be filed in the future.
                </p><br/>

                <h3>IJCRS Copyright Form:</h3>
                <br/>
                <p>It is required to obtain written confirmation from authors in order to acquire copyrights for papers published in the journal so as to index them to various repositories.
                </p><br/>
            </div>
            <a href="{{ asset('public/IJCRS Copyright Form.pdf') }}" download><h3>Click Here to Download IJCRS Copyright Form</h3></a><br/>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="faq_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">FREQUENTLY ASKED QUESTIONS (FAQ)...???</h1><br/>

               <p><strong>Q: What is International Journal of Creative Research and Studies (IJCRS)?</i></strong></p>
                <p><strong>A:</strong> International Journal of Creative Research and Studies (IJCRS) is a high quality open-accessed, peer-reviewed and refereed multidisciplinary research journal, dedicated to serve the society by the global dissemination of information through an unparalleled commitment to quality, reliability, and innovation and research work. </p><br>


               <p><strong>Q: Who is the publisher of IJCRS?</i></strong></p>
                <p><strong>A:</strong> IJCRS is published by Knowledge Ridge Publishers.</p><br>

               <p><strong>Q: What is the Scope of IJCRS?</i></strong></p>
                <p><strong>A:</strong> The journal publishes current and relevant findings from cutting edge research in the fields of Business, Economics & Management; Social Science, Literature, Arts & Humanities; Engineering & Technology; Life Science & Physical Science, Health & Medical Science and related issues.</p><br>

               <p><strong>Q: What types of paper are published in IJCRS?</i></strong></p>
                <p><strong>A:</strong> IJCRS welcomes and acknowledges high quality theoretical and empirical original research papers, case studies, review papers, literature reviews, book reviews, conceptual framework, analytical and simulation models, technical notes from researchers, academicians, professional, practitioners and students from all over the world.</p><br>

               <p><strong>Q: What is the frequency of publication of IJCRS?</i></strong></p>
                <p><strong>A:</strong> IJCRS is monthly journal. It publishes one issue per month.</p><br>

               <p><strong>Q: Is International Journal of Creative Research and Studies  (IJCRS) refereed journal?</i></strong></p>
                <p><strong>A:</strong> Yes, IJCRS is a refereed journal. It follows double blind review process.</p><br>

               <p><strong>Q: Is International Journal of Creative Research and Studies (IJCRS) an Open Access Journal?</i></strong></p>
                <p><strong>A:</strong> Yes, International Journal of Creative Research and Studies (IJCRS) is an international open access online journal. Thus, all articles published under open access can be accessed by anyone with internet connection without financial, legal or technical barriers.</p><br>

               <p><strong>Q: Is there a print version of the journal?</i></strong></p>
                <p><strong>A:</strong> Yes, IJCRS is published on-line and printed version.</p><br>

               <p><strong>Q: Is International Journal of Creative Research and Studies (IJCRS) having ISSN?</i></strong></p>
                <p><strong>A:</strong> Yes, ISSN of International Journal of Creative Research and Studies (IJCRS) is ISSN-0249-4655..</p><br>

               <p><strong>Q: How can I submit my manuscript to IJCRS?</i></strong></p>
                <p><strong>A:</strong> You can submit your paper to the editor through e-mail at:  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>submission@ijcrs.org or  </u></strong></a><span> or
							<span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.org.</u></strong></a><span></p><br>

               <p><strong>Q: Can I submit more than one paper for the same issue?</i></strong></p>
                <p><strong>A:</strong> Yes, you can submit more than one paper for the same issue.</p><br>

               <p><strong>Q: When should we submit our papers?</i></strong></p>
                <p><strong>A:</strong> Authors can submit the manuscripts anytime. It will be published in forthcoming issue, if accepted.</p><br>

               <p><strong>Q: When will I receive an acknowledgment email for my submission?</i></strong></p>
                <p><strong>A:</strong> Paper submissions are normally acknowledged within 2-3 days. In case the authors do not receive any acknowledgment for a submitted paper, an email can be sent to   <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.org.</u></strong></a><span>.</p><br>

               <p><strong>Q: I have not received any IJCRS notification even after 15 days of submission?</i></strong></p>
                <p><strong>A:</strong> Sometimes the notification email may be directed to your spam folder rather than your inbox. It is depend on your personal spam settings. If not so, please mention the issue to  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.org. </u></strong></a><span>. You request will proceed within 24 hours.</p><br>

               <p><strong>Q: Where can I find Author's Guidelines?</i></strong></p>
                <p><strong>A:</strong> You can find the Author's Guidelines in International Journal of Creative Research and Studies (IJCRS) Menu.</p><br>

               <p><strong>Q: Is there any restriction for number of pages and fee for extra pages?</i></strong></p>
                <p><strong>A:</strong> Minimum 5 pages and Maximum 25 pages are allowed. If it exceeds 25, remaining will be charged USD 5 per page.</p><br>

               <p><strong>Q: How many authors are allowed per paper? Is there a maximum limit for the number of authors?</i></strong></p>
                <p><strong>A:</strong> Yes, at present, maximum of FIVE authors are allowed per paper.</p><br>

               <p><strong>Q: How does the review process work?</i></strong></p>
                <p><strong>A:</strong>The review of articles, is done through a blind peer review. All the articles received by International Journal of Creative Research and Studies (IJCRS) are send to Review Committee after deleting the name of the author to have an unbiased opinion about the research.</p><br>

               <p><strong>Q: How much time does IJCRS take in review process?</i></strong></p>
                <p><strong>A:</strong>  The editorial board is highly committed to the quick review process of the paper, but not with the sacrifice of the right judgment of a paper. The review process takes maximum two weeks.</p><br>

               <p><strong>Q: How to correct an experimental data in my published paper?</i></strong></p>
                <p><strong>A:</strong> For any correction please contact the editorial team at  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u> submission@ijcrs.org.</u></strong></a><span>.Refer the changes providing the revised manuscript. In agreement to IJCRS article policies the revised manuscript will be published.</p><br>

               <p><strong>Q: How long does it take for an accepted paper to be actually published?</i></strong></p>
                <p><strong>A:</strong>  The actual date of publication is 1st of next month. It takes minimum 5 to 7 days for an accepted article to be published. We will intimate all the authors of a journal about the acceptance or rejection of an article. In order to publish the accepted article, any one of the author should transfer copyright form to IJCRS.</p><br>

               <p><strong>Q: How much do I have to pay for Publication Fee?</i></strong></p>
                <p><strong>A:</strong> If the paper is accepted for publication, author(s) will be asked to pay 100 USD as article publication fee. Waiver policy is not applicable.</p><br>

               <p><strong>Q: Will I get a hard copy of the journal?</i></strong></p>
                <p><strong>A:</strong> Author(s) will be entitled to one copy of the printed journal with free of charge i.e. one printed copy is provided against one article disregarding the number of the authors .Authors can also get additional copies of the printed journal by paying 30 USD for each additional copy.</p><br>

               <p><strong>Q: My paper has been accepted for publication. When do I pay Article Publication Fee?</i></strong></p>
                <p><strong>A:</strong>  Article Publication fee will have to be paid after the manuscript has been peer reviewed and accepted for publication. The corresponding author will be responsible for arranging the payments.</p><br>

               <p><strong>Q: How can I pay the Article Publication Charges?</i></strong></p>
                <p><strong>A:</strong> Preferred mode of Payment is to pay via - Paypal, Credit Cards, Bank Wire Transfer and Remittance Service like Western Union/ Xpress Money / Money gram / Trans fast / International Money Express. The details of how to make the payment will be sent to you after your article has been accepted for publication.</p><br>

               <p><strong>Q: My paper has been published, but I cannot find it on the Web?</i></strong></p>
                <p><strong>A:</strong> All published papers are normally indexed within 2-3 weeks from the date of publication and thereafter should normally be visible on the Web within 6-7 days.</p><br>
                <p><strong>Q: Does IJCRS provide any certificate of publication to the authors?</i></strong></p>
                <p><strong>A:</strong> Yes, after publication, IJCRS provides digitally signed e-certificate to the authors. If anyone wishes to get a printed certificate, he/she will have to pay USD 10 per copy of the certificate to meet the delivery charge.</p><br>

               <p><strong>Q: How to join the Reviewers Committee?</i></strong></p>
                <p><strong>A:</strong> : We welcome applications to join the Reviewers Committee. The possible reviewers have to communicate with the Chief Editor through E-mail by using <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.org</u></strong></a><span>. Please allow us 1-2 weeks for processing your application.</p><br>

               <p><strong>Q: How to join the Editorial Board Member?</i></strong></p>
                <p><strong>A:</strong> We welcome applications to join the Editorial Board. The possible editors have to communicate with the Chief Editor through E-mail by using  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.org.</u></strong></a><span>. Please allow us 1-2 weeks for processing your application.</p><br>

               <p><strong>Q: I did not find my question on your FAQ list.</i></strong></p>
                <p><strong>A:</strong>  Kindly send a mail to:  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>info@ijcrs.org</u></strong></a><span>.</p><br>


            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
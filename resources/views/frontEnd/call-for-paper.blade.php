@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="call_for_paper_content_area fix">
            <div class="call_for_paper_content fix">
                    <h1 style="text-align: center;margin-top: 10px;"><span class="call_paper_css_blink">Call For Paper</span></h1>
                <h1>International Journal of Creative Research and Studies (IJCRS)</h1>

    @foreach($paper_info as $paper)

                <h4>Submission open for:  <u><i>{{ $paper->call_for_paper_1 }}</i></u></h4><br>
                <h2>Important Dates:</h2>
                <h4>Deadline for Paper Submission: <u><i>{{ $paper->call_for_paper_2 }}</i></u></h4>
                <h4>Schedule of Publication: <u><i>{{ $paper->call_for_paper_3 }}</i></u></h4>

    @endforeach

            </div>
            <div class="call_for_paper_ijirk_text_1_1 fix">
                <p><b>IJCRS</b> cordially invites big achievers, researchers, scholars and authors to contribute their original works and illuminate the pages with their universal ideas and fresh perspectives to make the journal synonymous to the entire research field.</p>
            </div>
            <div class="call_for_paper_ijir_text fix">
                <p>International Journal of Creative Research and Studies (IJCRS) is a high quality open-access, peer-reviewed and refereed multidisciplinary research journal, dedicated to serve the society by the global dissemination of information through an unparalleled commitment to quality, reliability, and innovation and research work. IJCRS welcomes and acknowledges high quality theoretical and empirical original research papers, case studies, review papers, literature reviews, book reviews, conceptual framework, analytical and simulation models, technical notes from researchers, academicians, professional, practitioners and students from all over the world. IJCRS engages its noble efforts for the development and tries its best to give you better. IJCRS is much caring and faster way to publish your article without hesitation.
                </p>
            </div>
            <div class="call_for_paper_ijir_text_2 fix">
                <p><strong>AREA/SCOPE:</strong><br/>IJCRS publishes original research articles on a wide range of topics of contemporary relevance in the broad fields of Business, Economics and Management, Accounting and Finance, Social Science, Literature, Arts and Humanities, Engineering & Technology, Life Science and Physical Science, Health and Medical Science, and related issues.</p>
            </div>
            <div class="call_for_paper_ijir_text_3 fix">
                <p><strong>FREQUENCY & FORMAT:</strong><br/>International Journal of Creative Research and Studies (IJCRS) is published monthly. The Journal is published in both print and online version.</p><br/>

            </div>
                   <div class="call_for_paper_ijir_text_3 fix">
                <p><strong>PUBLICATION PROCESS:</strong><br/> The publication process is consisting with the following steps.<br/>
                 1. After receiving the manuscript we will send a confirmation e-mail to the author.<br/>
                 2. Primary quality will be checked by the Editor.<br/>
                 3. The manuscript will be sent for double blinded review.<br/>
                 4. Based on the double blinded review editor will take decision and the decision will be communicated to the author. Decisions can be three types like accepted without revision, resubmit with major/minor revisions, rejected.<br/>
                 5. After the final review process, if the paper will accepted we will send a payment request to the author.<br/>
                 6. Author need to deposit publication fee within 7 days after the payment request.</p><br/>  

            </div>
           
            <div class="call_for_paper_ijir_text_7 fix">
                <p><strong>Important Links:</strong></p>
                <p><a href="{{url('guideline-author')}}"><strong>Guidelines for Authors</strong></a></p>
                <p><a href="{{url('paper-submission')}}"><strong>Paper Submission</strong></a></p>
                <p><a href="{{url('review-process')}}"><strong>Review Process</strong></a></p>
                <p><a href="{{url('publication-fee')}}"><strong>Publication Fee</strong></a></p><br/>
            </div>
            <div class="call_for_paper_ijir_text_8 fix">
                <p><strong>SUBMIT YOUR PAPER:</strong></p>
                <p>Submit your manuscript directly to:<span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>submission@ijcrs.org </u></strong></a></span></p>
            </div>

        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
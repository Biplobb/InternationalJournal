@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="ethical_code_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">ETHICAL CODE</h1><br/>
                <h3 style="text-align: center;margin-top: 10px;">PUBLISHING ETHICS & MALPRACTICE STATEMENT:</h3><br/>


                <p>The publication of a research paper/case study/short communication in a peer-reviewed journal is an important building entity in the development of a knowledge repository. In an age of information abundance, it is very important and vital to help readers and research scholars to segregate quality information. It is mirror reflection of the quality work of the authors and the publishers who support them. International Journal of Creative Research and Studies (IJCRS) is committed to ensuring robust peer review and ethical standards in publication and quality of articles. It is mandatory for us to follow certain code of ethics and it is advices to adhere strictly to the following code of ethics which will enhance the quality of the published works heavily. IJCRS and its editorial board thrive to achieve the publication of articles by researchers and scholars. International Journal of Creative Research and Studies (IJCRS) understands the importance of publication ethics and we had established standards of ethical behavior for all parties involved such as the authors, the peer review experts, the editors.</p>
                <br/>
                <h3>AUTHORS AND CO-AUTHORS:</h3>
                <p>Authors are advised to follow the following code of ethics strictly. Submit manuscripts which are their original works or of the work they are associated with during their tenure. Submitted manuscripts should contain original and new results, data, and their ideas, which are not submitted for publishing to other publications or published elsewhere. Fabrication of data and results, intellectual property theft and plagiarism are highly unacceptable, it is beyond the ethics of an author. Information obtained via various media should be provided in the manuscript only with prior permission from the owner of the source of information or data. They should properly cite the work they are referring, authors are advised to cross check the reference before submission of manuscript. They may not promote in any form via any media to get their works published. No article should have an author who is not directly involved in the work for any means or reasons.<br/>
Authors and co-authors are requested to review and ensure the accuracy and validity of all the results prior to submission. Any potential conflict of interest should be informed to the editor in advance. Authors are bound by the Creative Commons licensing policy of publication. All authors are requested to submit the copyright transfer form & Undertaking form without failure once they receive the acceptance of their article for publication.


                </p>

                <h3>EDITORS:</h3>
                <p>Editors are the sole responsible persons for the acceptance or rejection of a manuscript, it may be subjected to peer review but the final decision is bound to the concerned editor. Any decision taken or matter of concern about a submitted article should not be revealed to anyone by an editor. If one of the editor is willing to publish an article the article should be processed by another editor. Editor should refrain from using the information, data, theories, or interpretations of any submitted manuscript in her/his own work until that manuscript is in press
                </p>
                
                <h3>REVIEWERS:</h3>
                <p>Reviewers are the main members contributing for the benefit of the journal being a peer reviewed (blind referee) journal they are insisted not to disclose their identity in any form. A reviewer should immediately decline to review an article submitted if he/she feels that the article is technically unqualified or if the timely review cannot be done by him/her or if the article has a conflict of interest. All submissions should be treated as confidential, editorial approval might be given for any outside persons advice received. No reviewer should pass on the article submitted to him/her for review to another reviewer in his own concern, it should be declined immediately. Reviewers being the base of the whole quality process should ensure that the articles published should be of high quality and original work. He may inform the editor if he finds the article submitted to him for review is under consideration in any other publication to his/her knowledge.
                </p>
                <h3>WHAT SHOULD BE CHECKED WHILE REVIEWING AN ARTICLE?</h3>
                <p>There are no hard and fast rules to analyze an article, this can be done on case to case basis considering the worthiness, quality, and originality of the article submitted. In general cases the following may be checked in a review:
                </p>
                <p>• Structure of the article submitted and its relevance to author guidelines</p>
                <p>• Purpose and Objective of the article</p>
                <p>•	 Method of using transitions in the article</p>
                <p>•	 Introduction given and the conclusion/ suggestions provided</p>
                <p>•	References provided to substantiate the content</p>
                <p>•	Grammar, punctuation and spelling</p>
                <p>•	Plagiarism issues</p>
                <p>•	Suitability of the article to the need</p>
                <p>•	Any conflict of interest that may be detected</p>
                <p>•	Knowledge addition to the scientific community</p>
                <p>•	Author(s) involvement in preparing the article and their interest shown towards scientific development.</p><br/>
                <p>A reviewer’s comment decides the acceptance or rejection of an article and they are one major element in a peer review process. All our reviewers are requested to go through the articles submitted to them for review in detail and give the review comments without any bias which will increase the quality of our journals.
                </p>
                

                <h3>BREACH OF CODE:</h3>
                <p>Being an association dedicated for the researcher fraternity, we all should ensure that the code of ethics formed is followed in all possible ways. Being a not-for-profit body it is the internal responsibility of a person whom should have to follow the codes, there is no enforcement to follow. The Executive Managing Editor of journal or committee members are entitled to take action against an individual if they found to be violating the code.
                </p>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
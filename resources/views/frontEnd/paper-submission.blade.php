@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="paper_submission_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">PAPER SUBMISSION</h1><br/>

                <p><strong>Submission Preparation Checklist</strong></p>
                <p>As part of the submission process, authors are required to check off their submission’s compliance with all of the following items, and submissions may be returned to authors that do not adhere to these guidelines.</p>
                <p>1.	The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).</p>
                <p>2.	The submission file is in Microsoft Word document file format</p>
                <p>3.	Where available, URLs for the references have been provided.
                <p>4.	The text is single-spaced type A4 pages, 1.15 line spacing; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.</p>
                <p>5.	The file should never exceed 25 pages/8000 words/8 MB owing to file restriction implied.</p>
                <p>6.	The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines, which is found in About the Journal.</p>
                <p>7.	If submitting to a peer-reviewed section of the journal, the instructions in Ensuring a Blind Review have been followed.</p>
                <br/>
                <p><strong>Submit Your Paper</strong></p>
                <p>Submit your manuscript directly to <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>: submission@ijcrs.org </u></strong></a></span></p>
                <br/>
            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
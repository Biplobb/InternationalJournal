<div class="content_right_area fix">
    <div class="content_right">
        <div class="ijirk_title_description fix">
            <div class="ijirk_title fix">
                <h3>IJCRS DESCRIPTION</h3>
            </div>
            <div class="ijirk_description fix">
                <p><strong>ISSN:</strong>0249-4655</p>
                <p><strong>Publisher:</strong>Knowledge-Ridge Publishers</p>
                <p><strong>Area/Scope:</strong>Business Economics and Management ;Social Science, Literature, Arts and Humanities; Engineering and Technology; Life Science and Physical Science; Health and Medical Science</p>
                <p> <strong>Frequency:</strong> Monthly</p>
                <p><strong>Format:</strong>Online & Print</p>
                <p><strong>Language:</strong>English</p>
                <p><strong>Review-Process:</strong>Double Blinded</p>
                <p>•<strong>Access:</strong> Open Access</p>

            </div>
            <div class="ijirk_description fix">
                <img  src="{{ asset('front-end/ijirk/img/ISSN.jpg') }}"  width="190px" height="100px">
                <img  src="{{ asset('front-end/ijirk/img/Publisher.jpg') }}"  width="190px" height="130px">
                <img  src="{{ asset('front-end/ijirk/img/Open.png') }}"  width="190px" height="130px">

            </div>
        </div><br/>

        <div class="sosial_image fix">
            <a href="https://www.facebook.com/IJCRSresearchJournal/?hc_ref=ARRk7OvHhUiWC9xBD-
EgwhZ47GSjhErWLnmJ4zo3ZD2KGHvD0o1RdnuxEbB6QfnW_dE" target="_blank"><img src="{{ asset('front-end/ijirk/images/fb.png') }}" alt="" /></a>
            <a href="https://twitter.com/IJCRSjournal" target="_blank"><img src="{{ asset('front-end/ijirk/images/twitter.jpg') }}" alt="" /></a>
            <a href="https://www.linkedin.com/in/ijcrs-research-journal-5bb29a157/" target="_blank"><img src="{{ asset('front-end/ijirk/images/in.jpg') }}" alt="" /></a>
            <a href="https://plus.google.com/u/0/110864605893596121368" target="_blank"><img src="{{ asset('front-end/ijirk/images/gg.jpg') }}" alt="" /></a>
        </div>
    </div>
</div>
<div class="footer_area fix">
    <div class="footer fix">
        <div class="footer_top fix">
            <img src="{{asset('front-end/ijirk/')}}/images/seperator1.png" alt="" />
        </div>
        <div class="footer_bottom fix">
            <p>Copyright &copy; 2017 IJCRS - International Journal of Creative Research and Studies (IJCRS). All Rights Reserved •</p>
        </div>
        
    </div>
</div>

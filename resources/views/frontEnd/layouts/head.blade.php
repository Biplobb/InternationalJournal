<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Home | IJCRS</title>
    <meta name="keywords" content="IJCRS,Home,About Us">
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/nivo-slider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/main_manue.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/marquee.css') }}">
    <link rel="shortcut icon" href="{{ asset('front-end/ijirk/images/logo-ijcrs.gif') }}" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/bar/bar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/default/default.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/light/light.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/nivo-slider.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front-end/ijirk/css/image-zoom.css') }}" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"></link>
    <script src="https://code.jquery.com/jquery-1.12.4.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js">
    </script>

</head>

<body background="{{asset('front-end/ijirk/images/intersection.png')}}">
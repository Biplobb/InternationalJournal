<div class="header_area">
    <div class="header">
        <div class="header_left fix">
            <div class="logo fix">
                <a href=#><img src="{{ asset('front-end/ijirk/images/logo-ijcrs.gif') }}" alt="" /></a>
            </div>
        </div>

        <div class="header_middle fix">
            <p>International Journal of Creative Research and Studies </p>

        </div>

    </div>
</div>
<div class="titless" style="overflow:hidden">
    <div class="title" style="float:right" >
        {!! Form::open(['route' => 'searchPDF','method'=>'post','class'=>'class_name']) !!}
        {!! Form::text('title', null, array('placeholder' => '           Search Text','id'=>'search_text')) !!}
        <select  id="searchField" name="searchField" size="1" class="combo_box">
            <option label="All" value="query">All</option>
            <option label="Title" value="title">Title</option>
            <option label="Authors" value="authors">Authors</option>



        </select>

         <button class="clickable" id="clickable">.        search    .</button>

   {{--{!! Form::submit('search') !!}--}}
        {!! Form::close() !!}
    </div>
</div>

<div class="main_menu_area">
    <div class="main_menu">
        <div id="manu">
            <ul id="menu">

                <li><a href="{{ URL::to('/') }}">Home</a></li>
                <li><a href="#">About Us</a>
                    <ul>
                        <li><a href="{{ URL::to('welcome_note') }}">WELCOME TO IJCRS</a></li>
                        <li><a href="{{ URL::to('aim_scope') }}">Aim & Scope</a></li>
                        <li><a href="{{ URL::to('mission_vision') }}">Mission & Vision</a></li>
                        <li><a href="{{ URL::to('policies') }}">Policies</a></li>
                        <li><a href="{{ URL::to('ethical') }}">Ethical Code</a> </li>
                        <li><a href="{{ URL::to('editors-member') }}">Editorial Board</a> </li>
                        <li><a href="{{ URL::to('publisher') }}">Publisher</a> </li>
                    </ul>
                </li>
                <li><a href="#">Authors Zone</a>
                    <ul>
                        <li><a href="{{ URL::to('author') }}">Rights and Obligations of Authors</a></li>
                        <li><a href="{{ URL::to('duties-author') }}">Duties of Authors</a></li>
                        <li><a href="{{URL::to('guideline-author') }}">Guideline for Authors</a></li>
                    </ul>
                </li>
                <li><a href="#">Editor & Reviewer Zone</a>
                    <ul>
                        <li><a href="{{ URL::to('duties-of-editor') }}">Duties of Editor and Reviewer</a></li>
                       
                    </ul>
                </li>
                <li><a href="#">Issues</a>
                    <ul>
                        <li><a href="{{ URL::to('current-issue') }}">Current Issue</a></li>

                        <li><a href="{{ URL::to('archive') }}">Archive Issues</a></li>
                    </ul>
                </li>
                <li><a href="{{ URL::to('journal-index') }}">Index</a></li>
                <li><a href="{{ URL::to('why-us') }}">Why Us<br/></a></li>
                <li><a href="{{ URL::to('faq') }}">FAQ</a></li>
                <li><a href="{{ URL::to('contact') }}">Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
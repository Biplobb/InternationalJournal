<div class="content_right_area fix">
    <div class="content_right">
        <div class="ijirk_title_description fix">
            <div class="ijirk_title fix">
                <h3>IJCRS DESCRIPTION</h3>
            </div>
            <div class="ijirk_description fix">
                <p>•<strong>ISSN:</strong> 2213-1356</p>
                <p>•<strong>Publisher:</strong> Scholar Touch Publishers</p>
                <p>•<strong>Area/Scope:</strong> Business, Economics & Management; Social Science, Literature, Arts & Humanities; Engineering & Technology; Life Science & Physical Science, Health & Medical Science</p>
                <p>•<strong>Frequency:</strong> Monthly</p>
                <p>•<strong>Format:</strong> Online & Print</p>
                <p>•<strong>Language:</strong> English</p>
                <p>•<strong>Review Process:</strong> Double Blinded</p>
                <p>•<strong>Access:</strong> Open Access</p>

            </div>
        </div>
        <div class="journal_cover_page_img fix">
            <a href="#"><img src="{{ asset('front-end/ijirk/images/cover.jpg') }}" alt="cover" /></a>
        </div>
        <div class="sosial_image fix">
            <a href="https://www.facebook.com/IJCRSresearchJournal/?hc_ref=ARRk7OvHhUiWC9xBD-
EgwhZ47GSjhErWLnmJ4zo3ZD2KGHvD0o1RdnuxEbB6QfnW_dE"><img src="{{ asset('front-end/ijirk/images/fb.png') }}" alt="" /></a>
            <a href="#"><img src="{{ asset('front-end/ijirk/images/twitter.jpg') }}" alt="" /></a>
            <a href="#"><img src="{{ asset('front-end/ijirk/images/in.jpg') }}" alt="" /></a>
            <a href="#"><img src="{{ asset('front-end/ijirk/images/gg.jpg') }}" alt="" /></a>
        </div>
    </div>
</div>
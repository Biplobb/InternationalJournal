<div class="sidebar_area fix">
    <div class="fix sidebar">
        <ul>
            <li><a href="{{ URL::to('/') }}">HOME</a></li>
            <li><a href="{{ URL::to('call-for-paper') }}">CALL FOR PAPER</a></li>
            <li><a href="{{ URL::to('paper-submission') }}">PAPER SUBMISSION</a></li>
            <li><a href="{{ URL::to('review-process') }}">REVIEW PROCESS</a></li>
            <li><a href="{{ URL::to('publication-fee') }}">PUBLICATION FEE</a></li>
            <li><a href="{{ URL::to('contact') }} ">CONTACT US</a></li>
        </ul>
        <div class="">
            <img id="myImg"  src="{{ asset('front-end/ijirk/img/Cover-Page.jpg') }}"  width="200px" height="400px">

        </div>
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
        </div>
    </div>

</div>
<script src="{{ asset('front-end/ijirk/js/image-zoom.js') }}"></script>
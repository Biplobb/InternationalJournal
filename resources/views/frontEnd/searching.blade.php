@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class=" fix content">
                <p></p><br>




            </div><br>
 @if(!empty($result->volumes))
            <h3><span class="current_issue_css_blink">{{$result->volumes}}</span></h3><br><br>
@endif
            @foreach($results as $issue)
              
                <div class="previous_issue_journal_list_area fix">
                    <div class="previous_issue_journal_list fix">
                        <div class="previous_issue_single_journal fix">
                            <p><strong>Title:</strong><a href="{{URL::to('/issue-details/'.$issue->id)}}"><span class="previous_issue_detail_paper"> {{$issue->title}}</span></a></p>
                            <p><strong>Author:</strong> {{$issue->author}}

                        </div>
                        <div class="previous_issue_single_journal_pdf fix">
                           <a href="{{ asset('/'.$issue->issue_image) }}" download><img src="{{asset('front-end/ijirk/')}}/images/pdf-icon.jpg" alt="pdficon" /></a>
                        </div>
                    </div>
                    <div class="seperator fix">
                        <img src="{{asset('front-end/ijirk/')}}/images/seperator1.png" alt="seperator" /></a>
                    </div>
                </div>
            @endforeach

        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>

</html>

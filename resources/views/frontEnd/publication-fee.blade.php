@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="publication_fee_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">PUBLICATION FEES</h1><br/>

                <p>The authors should note that there is no submission fee; however, there is a reasonable publication fee of each accepted article to meet the cost of server and website update and maintenance, Article handling, Review process, Printing, Electronic composition and production, hosting the final article on dedicated servers, electronic archiving, Office cum admin expenses and other recurring expenses.</p>

                <p>If the paper is accepted for publication, author(s) will be asked to pay <strong>100 USD</strong>  as article publication fee. Waiver policy is not applicable. Author(s) will be entitled to one copy of the printed journal with free of charge IE one printed copy is provided against one article disregarding the number of the authors. Authors can also get additional copies of the printed journal by paying <strong>30 USD</strong>  for each additional copy.</p>


                <p>•	The Journal accepts a Publication and Handling Fee only from Authors after their manuscripts has been accepted for Publication.</p><br/>

                <p><strong>DELIVERY OF THE HARD COPY’S</strong></p>
                <p>We use EMS service for the delivery purpose. It delivers the copy within 7-15 working days. This delivery fee is included in the publication fee. If any author wants to send his/her journal copy in DHL, FEDEX service, author will have to pay extra <strong>25 USD</strong> as delivery service fee.</p><br/>

               </p> <p><strong>PAYMENT INSTRUCTIONS</strong></p>
                <p>Preferred mode of Payment is to pay via -  <strong>Paypal,Credit Cards, Bank Wire Transfer </strong> <strong>Remittance Service</strong>  like like <strong>Western Union/ Xpress Money / Money gram / Trans fast / International Money Express.</strong> </p>
                <p><strong>Note:</strong> Account details will be sent if the paper is accepted for publication. Please provide all information to the accountant for receiving money.</p>



            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
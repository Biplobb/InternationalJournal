@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class=" fix content">
                <p></p><br>


                <h2>{{ $issue_info->volumes }}</h2><br>

        </div><br>



                <div class="previous_issue_journal_list_area fix">
                    <div class="previous_issue_journal_list fix">
                        <div class="previous_issue_single_journal fix">
                          <p><strong>Title:</strong><span class="previous_issue_detail_paper"> {{$issue_info->title}}</span></p>
                            <p><strong>Author:</strong> {{$issue_info->author}}</p>
                            <div id="articleAbstract">

                                <br />
                                <div><p><strong>Abstract:</strong><em><?php echo str_limit($issue_info->details,1000)  ?></em></p></div>
                                <br />
                            </div>
 <p>For Details Click Here:<a href="{{ asset('/'.$issue_info->issue_image) }}" download> <img src="{{asset('front-end/ijirk/images/other/pdf.PNG')}}" class="pdf_icon"></a>
                        

                        </div>



                    </div>

                </div>



        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>

</html>
@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="guideline_for_author_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;"> GUIDELINES FOR AUTHORS</h1><br/>

                <p>The manuscripts in all the subject areas are welcome. Moreover, submitted manuscript must not be previously accepted for publication elsewhere. The authors should ensure that they have written entirely original works, and if the authors have used the work this has been appropriately cited or quoted.</p><br>

                <p>Authors are advised to submit the paper/article electronically as attachment in (submit your paper) as per all instructions following as under.</p><br>
                <p>Language: Language of the articles should be only in English.
                </p><br>
                <p><strong>File Type:</strong> Authors should submit the articles only in MS-word format,</p><br>
                <p><strong>Page Format:</strong> Single/Double column – single spaced type A4 pages, 1.15 line spacing, Moderate margin.
                </p><br>
                <p><strong>Font Type:</strong> TIMES NEW ROMAN</p><br>
                <p><strong>For main heading or title:</strong> 18 points and Bold, for subtitles: 14 points and Bold, Content of article: 12 points, theory: 12 points.</p><br>
                <p><strong>Size of Submission:</strong> Authors are instructed to restrict the number of pages not more than to avoid ambiguity and reduce the editing time. The file should be attached as email attachment. In any case the size of the file should never exceed 25 pages/8000 words/8 MB owing to file restriction implied..</p><br>


                <p>The submitted paper should contain (author may add subtitle as per their matter/topic)</p><br>
                <p>1.	Title of the Paper</p>
                <p>2.	Author’s Name, Email, Contact Number, Designation, University/College, State, Country.</p>
                <p>3.	Abstract</p>
                <p>4.	Keywords or Phrases (4 to 10)</p>
                <p>5.	Introduction</p>
                <p>6.	Materials and Methods</p>
                <p>7.	Findings and Analysis</p>
                <p>8.	Recommendations</p>
                <p>9.	Conclusions</p>
                <p>10.	List of References or Bibliography</p>
                <p>11.	Author’s Biography (Maximum 300 words)</p>
                <br><br>
                <p><strong>Submission Order:</strong></p>

                <p>Page 1 should be consisting following:</p>

                <p>•	Related subject/Field ( Branch of study)</p>
                <p>•	Title of the paper</p>
                <p>•	Author name(s), Designation, Department, Institute/University, State, Country.</p>
                <p>•	Address (es)</p>
                <p>•	Phone number</p>
                <p>•	E-mail address (es)</p>
                <br><br>
                <p><strong>From page 2</strong> consist main point with single column in single spaced typed format (All points/sub titles can be as per different subject and topic of research so; Author may use contents, wording matter, charts, etc. as per their subject and related topic)</p><br>

                <p><strong>Abstract:</strong> This section should detail the problems, experimental approach, major findings and conclusion in one paragraph and should appear on the second page. The abstract should be 100 to 250 words in length. Complete sentences, active verbs and the third person should be used. The abstract should be written in past tense. Standard nomenclature should be used and abbreviations should be avoided.  Avoid abbreviation, diagram and references in the abstract.</p><br>

                <p><strong>Keywords:</strong> Author(s) must give 4 to 10 key words which can identify the most important subjects covered by the paper. They must be placed at the end of the abstract. A list of non-standard abbreviations should be added. In general, non-standard abbreviations should be used only when the full term is very long and used often. Each abbreviation should be spelt out and introduced in parentheses the first time it is used in the text. Only recommended SI units should be used. Authors should use the solidus presentation (mg/ml), Standard abbreviations (such as ATP and DNA) need not be defined.</p><br>

                <p><strong>Introduction:</strong>  The introduction should set the tone of the paper by providing a clear statement of the study, the relevant literature on the study subject and the proposed approach or solution. The introduction should be general enough to attract a reader’s attention from a broad range of scientific disciplines, abbreviations should be included. The manuscript should include the purpose of the investigation and relating the manuscript to similar previous research. Only information essential to the arguments should be presented.</p><br>

                <p><strong>Materials and Methods:</strong> This section should provide a complete overview of the design of the study. Detailed descriptions of materials or participants, comparisons, interventions and types of analysis should be mentioned. However, only new procedures should be described in detail; previously published procedures should be cited and important modifications of published procedures should be mentioned briefly. Capitalize trade names and include the manufacturer’s name and address. This section must contain specific details about the materials studied, instruments used, specialized source and related experimental details which allows other research worker to reproduce the results. Obtain permission for all fully borrowed, adapted, and modified tables and provide a credit line in the footnote. Results and Discussions The results should be concisely presented. Results and discussion may be separate or combined based on the author’s requirement. Tables and figures should be designed to maximize the comprehension of the experimental data. The interpreted results should be explained clearly in discussions and should relate them to the existing knowledge in the field as clearly as possible. Tables, Graphs and figures (Illustrations) should be inserted in to the main text at respective place they should appear when published and should have appropriate numbers and titles with an explanatory heading. Labels of the table, graph and figures MUST be in the text form and should not form part of the image. Colour photographs and illustrations (line drawings, halftones, photos, photo micro-graphs etc.) must be clean originals or digital files would be charged that may be intimated along with the acceptance letter. Those photographs must be clear and sharp. Digital files are recommended for highest quality reproduction.</p><br>

                <p><strong>The Discussion</strong> should interpret the findings in view of the results obtained in this and in past studies on this topic. State the conclusions in a few sentences at the end of the paper. The Results and Discussion sections can include subheadings, and when appropriate, both sections can be combined.</p><br>

                <p><strong>Results</strong> should be presented with clarity and precision. The results should be written in the past tense when describing findings in the author(s)’s experiment. Previously published findings should be written in the present tense. Results should be explained, but largely without referring to the literature. Discussion, speculation and detailed interpretation of data should not be included in the results but should be put into the discussion section.</p><br>

                <p><strong>Acknowledgement (if any):</strong> This section can be kept at the end of the manuscript before reference section. This section can be used to acknowledge the help of those who do not qualify for authorship or to acknowledge funding, donated resources or significant contribution to the research.</p><br>

                <p><strong>References:</strong> References to the literature cited for the manuscript should be numbered in order of appearance in the manuscript and cited in the text with superscript numbers. The reference number should follow the following format.</p><br>

                <p>1.	Amie UK, Cleavage of structural proteins during the assembly of the head of bacteriophage T4. Nature 227: 680-685, 1970.
                    2.	R., and Ramesh G.R,  Article title, Journal of American Society of Civil Engineers, 23(2),pp 12-15, 2008.</p><br>
                <p><strong>OR</strong></p><br>
                <p>1.	Amie UK (1970): Cleavage of structural proteins during the assembly of the head of bacteriophage T4. Nature 227: 680-685.
                    2.	R., and Ramesh G.R,(2008): Article title, Journal of American Society of Civil Engineers, 23(2),pp 12-15.</p><br>

                <p><strong>Books :</strong></p>
                <p>1.	Gupta., Introduction to Operations Research, 3rd. Ed. McGraw Hill Inc, 34-96. (2006).
                    Book chapter: Botkin,J,  What do you meant by Ecosystem in : Environmental Science of Living Plater, 3rd edition, E-age Publishing, New York. (2006).</p><br>

                <p><strong>Conferences :</strong></p>
                <p>1.	B,”Geospatial technology for disaster management”, International Conference on Disaster Mitigation and Management, PSNA College of Engineering and Technology, Dindigul, pp 234-238</p><br>

                <p><strong>Web pages :</strong></p>
                <p>1.	Ramesh J,  How to draw in Computers. IOP Publishing Physics Web.(2007).http://company.org/articles/news/11/6/16/2. Accessed 26 June 2010.</p><br>

                <p><strong>Tables</strong> should be kept to a minimum and be designed to be as simple as possible. Tables are to be typed double-spaced throughout, including headings and footnotes. Each table should be on a separate page, numbered consecutively in Arabic numerals and supplied with a heading and a legend. Tables should be self-explanatory without reference to the text. The details of the methods used in the experiments should preferably be described in the legend instead of in the text. The same data should not be presented in both table and graph forms or repeated in the text.</p><br>

                <p><strong>Figure</strong> legends should be typed in numerical order on a separate sheet. Graphics should be prepared using applications capable of generating high resolution GIF, TIFF, JPEG or PowerPoint before pasting in the Microsoft Word manuscript file. Tables should be prepared in Microsoft Word. Use Arabic numerals to designate figures and upper case letters for their parts (Figure 1). Begin each legend with a title and include sufficient description so that the figure is understandable without reading the text of the manuscript. Information given in legends should not be repeated in the text.</p><br>

                <p><strong>Equations:</strong></p>
                <p>All equations should be numbered in order from starting and the notations used in the various equations should be given after abstract. Authors are insisted to follow standard notations and SI units system even though it’s not mandatory.</p><br>

                <p><strong>Images:</strong></p>
                <p>Images should be supplied without being inserted in tables, borders or any other format, they can be copy pasted in the required space. It will be better if the images are given in the last page, the quality of the image and the size should be proper.</p><br>

               <h4><a href="{{ asset('public/IJCRS Paper Template.pdf') }}" download style="text-align: center;">Click Here to Download the IJCRS Paper Template</a></h4><br>

                <p><strong>Submission Email:</strong>  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>submission@ijcrs.org</u></strong></a><span>   /   <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.org</u></strong></a><span></p>



            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
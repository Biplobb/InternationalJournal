@include('frontEnd.layouts.head')
<style type="text/css">
    p{
        color:black;
    }
</style>

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="aim_and_scope_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">AIM & SCOPE</h1>
             
              <h4 style="text-align: center"> <strong>AIM</strong> </h4> 
 <p>The <strong>IJCRS</strong>  aims at the creation, dissemination and re-invention of knowledge, emphasised by the quintessence of extraordinary and innovative research works. Our multi-dimensional mission is to promote awareness of and compatibility with the dynamics of study area among scholars, academicians, professors, and proponents.
The aim of IJCRS is to publish current and relevant findings from cutting edge research in the fields of Business, Economics and Management, Accounting and Finance, Social Science, Literature, Arts and Humanities, Engineering & Technology, Life Science and Physical Science, Health and Medical Science, and related issues.</p>
<br/>


<h4 style="text-align: center"><strong>SCOPE</strong></h4>
<p>International Journal of Creative Research and Studies is published monthly. The Journal is published in both print and online version. International Journal of Creative Research and Studies publishes research paper in the field of:</p><br/>

<h4 style="text-align: center"><strong>Business, Economics & Management:</strong></h4>
<p> <strong>The below mentioned areas are just indicative (the areas are not limited to the following):</strong><br/> 
General Management, Accounting & Taxation, Strategy, Business and Marketing, Executive Management, Organization Studies, Human Resources Management, Crisis Management, National Resources Management, Advertising, Advertising and Promotions Management, Brand Management, Consumer Behavior & Marketing Research, Knowledge management, Portfolio Management, Business Administration, Public Administration, Enterprises Recourse Management, Business Ethics, Managerial Economics, Marketing Management, Operations Management, Organizational Behavior, Business Law and Corporate Governance, Corporate Strategy, International Marketing, Entrepreneurship, Strategic Agribusiness Management, Training and Development, Data Structure and Algorithms, E-Business, E-Commerce, E-Governance, Financial Management, Internet applications and performances, Knowledge based systems, Management information systems, Mobile and Wireless Networks, Financial Studies and Markets, Banking and Finance, Commercial Banking, Financial and Management Accounting, Corporate Finance, Business studies, ethics, Education issues, entrepreneurship, electronic markets, Services, strategic alliances, Microeconomics, Government regulation, taxation, law issues, Macroeconomics, Financial markets, investment theories, International economics, FDI, Economic development, system dynamics, emerging markets, Empirical studies, quantitative/experimental methods, Marketing theory and applications, Business finance and investment, General business research, Business and economics education, Production/operations management, Organizational behavior and theory, Strategic management policy, Social issues and public policy, Management organization, Statistics and econometrics, Personnel and industrial relations, Technology and innovation, Case studies, and management information systems. </p> <br/>

<h4 style="text-align: center"><strong>Social Science, Humanities, Literature & Arts:</strong></h4>
<p> <strong> The below mentioned areas are just indicative (the areas are not limited to the following):</strong><br/>
Literature & Writing, English Language & Literature, Humanities, Literature & Arts (general), Visual Arts, Epistemology & Scientific History, Language & Linguistics, Sociology, Social Welfare, Foreign Language Learning, Music & Musicology, Feminism & Women's Studies, Communication, Gender Studies, Religious Studies, Drama & Theater, Film, Arts, History, Sex & Sexuality, Political Science, Economic History, Geography & Cartography, Public Health, Anthropology, Psychology, Philosophy, Education, Health Policy & Medical Law, Public Policy & Administration, Archaeology, Educational Administration, Higher Education, Architecture, Educational Psychology & Counseling, History, Educational Technology, Human Migration, Social Sciences (general), Bioethics, Environmental & Occupational Medicine, Human Resources & Organizations, Social Work, Asian Studies & History, Middle Eastern & Islamic Studies, African Studies & History, Ethnic & Cultural Studies, Latin American Studies, American Literature & Studies, Canadian Studies & History, Chinese Studies & History, French Studies, Academic & Psychological Testing, Early Childhood Education, Forensic Science, Environmental Law & Policy, International Law, Sociology, Epistemology & Scientific History, Law, Special Education, Cognitive Science, Ethics, Library & Information Science, Sustainable Development, Criminology, Criminal Law & Policing, European Law, Teaching & Teacher Education, Development, Economics, Family Studies, Military Studies, Technology Law, Diplomacy & International Relations, Feminism & Women's Studies, Paleontology, Urban Studies & Planning and so on. </p> <br/>

<h4 style="text-align: center"> <strong>Engineering & Technology:</strong> </h4>
 <p> <strong>The below mentioned areas are just indicative (the areas are not limited to the following):</strong>  <br/>
Computer Science and Engineering, Electrical Engineering, Civil Engineering, Infrastructure Engineering, Electronics & Communication Engineering, Industrial Engineering, Mechanical Engineering, Manufacturing Engineering, Graphics and Multimedia, Software Engineering, Biological Engineering, Chemical Engineering, Biomedical Technology, Biotechnology, Agricultural engineering, Remote sensing and information system, Textile Engineering, Robotics, Microelectronics & Electronic Packaging, Automation & Control Theory. </p> <br/>

<h4 style="text-align: center"><strong>Life Science & Physical Science:</strong></h4>
<p> <strong>The below mentioned areas are just indicative (the areas are not limited to the following):</strong> <br/>
Biological Sciences, Biochemistry, Genetics, Environmental Science, Immunology and Microbiology, Cellular Microbiology, Environmental Microbiology, Medical Microbiology, Industrial Microbiology, Soil and Agricultural, Microbial Genetics, Microbial Ecology, Vermitechnology, Medicinal Plants, Biotechnology, Pharmacology Science, Botany, Cellbiology, Conservation biology, Developmental biology, Ecology, Evolutionary biology, Evolutionary genetics, Food science, Genomics, Immunology, Marine biology, Molecular biology, Parasitology, Pathology, Pharmacogenomics, Pharmacology, Physiology, Population dynamics, Proteomics, Structural biology, Systems biology, Zoology, Chemical Engineering, Chemistry, Earth and Planetary Science, Energy, Mathematics, Physics and Astronomy, Engineering, Material Science, Statistics, Comparison of chemistry and physics, Natural and physical sciences, Earth science, Biophysics, Mathematics and Computer Science Research, Pure and Applied Chemistry, Geology and Mining Research, Environmental Chemistry and Ecotoxicology, Internet and Information Systems, Oceanography and Marine Science, Petroleum Technology and Alternative Fuels. </p> <br/>

<h4 style="text-align: center"> <strong>Health & Medical Science:</strong> </h4>
<p> <strong>The below mentioned areas are just indicative (the areas are not limited to the following):</strong> <br/>
Medicine and Dentistry, Nursing and Health Professions, Pharmacology and Toxicology, Pharmaceutical Science, Veterinary Science, Veterinary Medicine, Nutrition and Metabolism, Cancer Research and Experimental Oncology, Clinical Pathology and Forensic, Medicine, Diabetes and Endocrinology, Infectious Diseases and Immunity, Medical Laboratory and Diagnosis, Neuroscience and Behavioral Health, Public Health and Epidemiology, Medical Case Studies, Medical Practice and Reviews, Radiology & Medical Imaging, Emergency Medicine, Nuclear Medicine, Radiotherapy & Molecular Imaging, Health & Medical Sciences (general), Primary Health Care, Rehabilitation Therapy, Transplantation.</p>

            </div>
        </div>
       @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
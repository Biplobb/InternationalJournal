@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="review_process_ijirk_text fix">

                <h1 style="text-align: center;margin-top: 10px;">PEER REVIEW PROCESS</h1><br/>
                <p><strong>WHAT IS PEER REVIEW...?</strong></p>
                <p>Peer review is a process of self-regulation by a profession or a process of evaluation involving qualified individuals within the relevant field. Peer review methods are employed to maintain standards, improve performance and provide credibility. In academia, peer review is often used to determine an academic paper's suitability for publication. In case of blind peer review, the identity of the authors would not be revealed to reviewer/referee till the paper is published in the journal.</p>

                <p><strong>WHY PEER REVIEW…?</strong></p>
                <p>It is difficult for authors and researchers, whether individually or in a team, to spot every mistake or flaw in a complicated piece of work. This is not necessarily a reflection on those concerned, but because with a new and perhaps eclectic subject, an opportunity for improvement may be more obvious to someone with special expertise or who simply looks at it with a fresh eye through different angle. Reviewing of the research of others can identify the possible weaknesses / flaw, thus quality would be improved. For both grant-funding and publication in a scholarly journal, it is also normally a requirement that the subject is both novel and substantial.</p>

                <p><strong> <i> IJCRS publishes Original and High Quality Research / Technical Papers only.</i> </strong></p>


                <p><strong>BLIND PEER REVIEW BY EXPERT PEER REVIEWERS</strong></p>

                <p>We send the original, unpublished article for review by expert in the related field for “double blind” review process: authors are not told who reviewed their paper and reviewers are not told who wrote the paper. The reviewers give their advice on publication opportunity, together with their observations to the editor in chief, which will transmit them to the authors. If the referents have contrary opinions, the editor in chief may ask the opinion of a third referent. Our editorial take decision after the review is complete. This review is adopted in case of scientific/ technical new research paper.
                    On topic papers will be primary tested for plagiarism. If the paper is suspected of plagiarism, it will be automatically rejected.</p>
                <p>If paper accepted we send notification to the corresponding author should remit the final format of the accepted manuscript within three working days and authors have to complete all process of copyright agreement.</p>

                <p><strong>Every Manuscript submitted to IJCRS will be subjected rigorous process as follows:</strong></p>

                <p>1) [Author=>> Editor]
                    Author has to submit his / her manuscript to the Editor through online submission.</p>

                <p>2) [Editor / Review Members]</p>
                <p>- We determine the quality and originality of the submitted manuscript content by doing plagiarism (two times by two independent people).</p>
                <p>- The manuscript would be sent to two individual review members for the comments.</p>
                <p>- Editor / Review Members gives his / her first opinion on particular manuscript, it is accepted / rejected.</p>

                <p>3) [Editor]</p>
                <p>- If manuscript accepted, the Editor will send it to two individual reviewers</p>
                <p>- If manuscript rejected, the Editor regret message to the authors.</p>

                <p>4) [Reviewer=>> Editor]</p>
                <p>Reviewer team will evaluate the manuscript and gives the status to the Editor as follows:</p>
                <p>i.   Manuscript may be Accepted</p>
                <p>ii.   Manuscript may be Accepted with minor changes</p>
                <p>iii.   Manuscript may be Accepted with major changes</p>
                <p>iv.   Manuscript may be Rejected</p>

                <p>5) [Editor]</p>
                <p>- Editor sends the evaluation report to author through an e-mail.</p>
                <p>- Editor also gives concerned bank account detail for publication fee, if manuscript is accepted.</p>

                <p>6) [The corresponding Author]</p>
                <p>The corresponding author should take one of the following actions according to the evaluation report</p>

                <p>i.  Accepted</p>
                <p>- Deposit publication fee to the given bank account by thoroughly checking with bank at the time of deposit.
                    Misconception  will not be entertained.</p>
                <p>- Sends final manuscript, Copyright form and Payment details (scanned copy of payment slip / online transaction details) to  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.com</u></strong></a><span></p>

                <p>ii.  Accepted with changes</p>
                <p>- Make changes in manuscript and re-submit to editor through e-mail.</p>

                <p>iii. Rejected for revision/resubmission</p>
                <p>- Do the revision, make changes in manuscript and re-submit to editor</p>

                <p>iv.   Rejected</p>
                <p>-  Author can submit a new article for forthcoming issues.</p>

                <p>7) [Editor=>> Publisher]</p>
                <p>Editor will send the final manuscripts to Publisher to publish in the current issue</p>

                <p>8) [Publisher]</p>
                <p>Publisher will publish the accepted manuscript in the current issue.

                <p>9) [Editor]</p>
                <p>Editor gives the published information to the corresponding by e-mail.</p>



                <p><strong>Sources of Peer Reviewers</strong></p>

                <p>Reviewers are drawn from the journal’s editorial board and volunteer peer reviewer pool.  Occasionally reviewers identified as being suitable through different sources will be approached.</p>

                <p><strong>Note:</strong> Recommendations from authors of peer reviewers from other institutions is accepted and the authors need to send us the name, affiliations and official email id and contact no.</p>




            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
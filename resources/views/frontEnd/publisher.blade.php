@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="publishers_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;">KNOWLEDGE RIDGE PUBLISHERS</h1><br/>

                <p> <strong> Knowledge Ridge Publishers</strong> is an independent academic publishing company, well known worldwide for its innovations in the field of semantic publishing and for its cutting-edge publishing tools and workflows. Founded in 2010 and initially focusing on book publishing, it has grown to become a leading publisher that specializes in publishing innovative scholar journals, academic books. With the dream that our each and every reader around the world can have equal and free access to our ever growing knowledge pool, Knowledge Ridge Publishers adheres to the Open Access principles, under which, everyone can get full journal article contents from our website without registration or any kind of charge.</p>

                  <p>  Knowledge Ridge Publishers is a member or partner of several professional publishing organisations and data publishing platforms. The company is actively developing novel tools, workflows and methods for text and data publication and dissemination of scientific information, as well as technologies for semantic enrichment of content. Knowledge Ridge Publishers offers a variety of programs to support and promote education development, international cooperation. </p>

                    <p> We have been seeking a way to scrap the notion of academic publishing being a time-consuming and exhausting venture for any author ready to present a research. We are always keen to discuss new publishing projects. The belief is deeply rooted in our organization that none of what Knowledge Ridge Publishers has achieved today would ever be made possible without your support and contribution, and it is with great appreciation you would continue to play an important and invaluable role in our academic community as a reader, author, editor or just to spread the words.</p>
                </p>
                <br>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>
</html>
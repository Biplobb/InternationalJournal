@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="duties_editors_ijirk_text fix">
                <h1 style="text-align: center;margin-top: 10px;"> DUTIES OF EDITOR AND REVIEWER</h1><br/>

                <p><strong>IJCRS</strong>  publishes peer-reviewed journals on various disciplines. This statement explains ethical behaviour of all parties involved in the act of publishing an article for journal, i.e.: the author, the Executive Editor, Advisor, the reviewer and the publisher. This statement is based on internationally accepted Best Practice Guidelines for Journal Editors.</p><br/>

                </p>
                <h4 style="text-align: center;"><strong>DUTIES OF EDITORS:</strong></h4>
                <p><strong>Decision on the Publication of Articles:</strong></p>
                <p>The Executive Editor of each Journal is responsible for deciding which of the articles submitted to the journal should be published. The Executive Editor may be guided by the policies of the journal’s editorial board and subjected to such legal requirements regarding libel, copyright infringement and plagiarism. The Executive Editor may confer with other editors or reviewers in making this decision.
                </p><br/>
                <p><strong>Fair play:</strong></p>
                <p>Manuscripts shall be evaluated solely on their intellectual merit without regard to authors’ race, gender, sexual orientation, religious belief, ethnic origin, citizenship, or political philosophy.
                </p>
                <br/>
                <p><strong>Confidentiality:</strong></p>
                <p>The Editor in Chief/editors and any editorial staff must not disclose any information about a submitted manuscript to anyone other than the corresponding author, reviewers, potential reviewers, other editorial advisers, and the publisher.
                </p><br/>
                <p><strong>Disclosure and conflicts of interest:</strong></p>
                <p>Unpublished materials disclosed in a submitted manuscript must not be used by anyone who has a view of the manuscript (while handling it) in his or her own research without the express written consent of the author.
                </p><br/>

                <h3>DUTIES OF REVIEWERS:</h3>
                <p>Reviewer should review and send the review comments in due time period. If the article is not in your area of interest then revert back to editor so that the other reviewers can be approached.</p><br/>
                <p>‘The IJCRS’ follows <strong>Double Blinded Review</strong> process under the Peer Review. In this process both the reviewer and the author remain anonymous to each other.
                    In the abstract field of knowledge, setting a parameter could be incongruous with the immediate thought. But, one has to follow a certain norm while adjudging an article, when it comes to precision and appraisal. These are as follows:-
                </p><br/>
                <p><strong>Confidential:</strong></p>
                <p>Confidentiality is a key of expertise. Thus we recommend a secret review process that is not shared between the reviewer and the author. This is entirely an individual product of the reviewer, where the judgement is made upon certain yardsticks, essential to a collective research area. The editor, sometimes can be the collaborator in few cases but he certainly should not divulge out any bit of information to the author. Professional austerity is desirable.

                </p><br/>

                <p><strong>Blind review:</strong></p>
                <p>Blind review is meant for fair-dealing. By a logical extension of the above point, this very unique blind review process is launched where the reviewer is made entirely obliterated of the authors’ identity and whereabouts. This process serves to assure uniformity, impartiality and constraint on the part of the reviewer.

                </p><br/>
                <p><strong>Stipulated time:</strong></p>
                <p>We put high value on time. Since the journal provides a rapid course with the publication, the entire review process is a meticulous analysis within a restricted time period. With a touch of skill and knowledge, the reviewer has to adhere to a given deadline.

                </p><br/>
                <p><strong>Originality:</strong></p>
                <p>We are synonymous with authenticity. Keeping relevance with the context, the submission has to be original in words and in idea. Reviewer should judge an article with respect to its genuineness. It has to add wisdom to knowledge and views to news. Every article should possess some exponential substance within it. If the research finding collides with one of a time ahead, the article should immediately be passed on to the editor mentioning the reference portion along with its unique manuscript id.

                </p><br/>
                <p><strong>Structure:</strong></p>
                <p>We aim at structural review. As skeleton to human body, so is structure to an article. It helps the contents fit to its contour by opting for an amalgam of the stuffing and the frame. Each article has to be laid out in alliance with the convention where all the relating parts like abstract, introduction, methodology, results, conclusions, reference and bibliography, are in proper consistency.
                </p><br/>
                <p><strong>Implementability:</strong></p>
                <p>Examples are better than precepts and examples are even wittier when they are exercised. Dealing with research and innovation, an article has to give an apt eye to the practicability of its proposition.  The suggestions have to be implemental than being only passively manipulative.
                </p><br/>
                <p><strong>Research:</strong></p>
                <p>A multi-disciplinary journal has an extensive research area. The articles are invited from different arenas of academics, management and technology with a view to create and disseminate knowledge, but the reviewer is not supposed to lose his/her foothold in the labyrinth of multiple studies. These articles should be reviewed following the conventional research methodology.
                </p><br/>
                <p><strong>Language:</strong></p>
                <p>Language is the career of idea. So it needs to be well-structured, simpler and familiar in order to merge with a lucid style of paraphrasing the different beneficial points and comments. Therefore, a smart diction is desirable on the part of the reviewer.
                </p><br/>
                <p><strong>Conflict of Interest:</strong></p>
                <p>Reviewers should not review manuscripts in which they have conflicts of interest resulting from competitive, collaborative, or other relationships or connections with any of the authors or institutions connected to the papers.
                </p><br/>
                <p><strong>Editors and reviewers can contact to the Executive Editor through:</strong></p>
                <p><span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.com</u></strong></a><span></p><br/>


                <h3>JOIN AS A REVIEWER</h3>
                <p>We welcome scholarly educationalists/expert from different disciplines to join us as well as put their efforts to put research paper and scholarly articles from their group/colleagues.
                    The registered experts will receive the sort listed articles for review. We hope to get the assistance of the reviewers in making the publication smooth and beneficial to the community of the scholars and researchers.
                </p><br/>
                <p><strong>Criteria to Join our Editorial Board – </strong> </p><br/>
                <p><strong>Associate Editor – </strong>Minimum 8 years of Exposure in Research work and Teaching field with more than 20 publications. Associate Editors have to take final decision of reviewed articles with comments.
                </p><br/>
                <p><strong>Reviewer – </strong>Minimum 5 years of Exposure in Research work and Teaching field with more than 15 publications.
                </p><br/>

                <p><strong>Confidential:</strong></p>
                <p>Confidentiality is a key of expertise.  Thus we recommend a secret review process that is not shared between the reviewer and the author. This is entirely an individual product of the reviewer, where the judgement is made upon certain yardsticks, essential to a collective research   area. The editor, sometimes can be the collaborator in few cases but he certainly should not divulge out any bit of information to the author. Professional austerity is desirable.
                </p><br/>
                <p>Reviewer should review and send the review comments in due time period. If the article is not in your area of interest then revert back to editor so that the other reviewers can be approached.
                    You need to work for the benefits of the larger community of the researchers and scholars, your presence and support will enhance our morale for serving the society through sharing of knowledge free of charge and making your expertise and experience known to the world.
                </p><br/>

                <h4>To register, send full details to –  <span class="all_email_hover"><a href="https://mail.google.com" target="_blank"><strong><u>editor@ijcrs.com</u></strong></a><span></h4>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
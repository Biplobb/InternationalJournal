@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="why_us_ijirk_text fix">

                <h1 style="text-align: center;margin-top: 10px;">Why Us</h1><br/>
                <p>The authors sending their articles or papers for publication in IJCRS will have the benefits as mentioned here under.</p><br>
                <p>•	Best Global Knowledge Sharing Platform</p>
                <p>•	Originality and Innovation</p>
                <p>•	International Quality and Standards</p>
                <p>•	Encouraging Quality Research</p>
                <p>•	360 Degrees Coverage Of Research Field</p>
                <p>•	Rapid Review Process</p>
                <p>•	Rapid Publication Process</p>
                <p>•	High quality Open access Journal</p>
                <p>•	Effective Editorial Standards</p>
                <p>•	Both Online and Print Version</p>
                <p>•	Rigorous Peer Review Process</p>
                <p>•	Strong National and International network & Collaboration</p>
                <p>•	Broad visibility through Promotion and Distribution</p>
                <p>•	Papers abstract / indexed by all the major scientific indexing services</p>
                <p>•	Reliable, Easy and Rapidly growing Publication with nominal publication fees</p>
                <p>•	High level of commitment</p>
                <p>•	committed to the achievement of technical quality and excellence in our programs, Publications, courses, and other activities</p>
                <p>•	committed to honesty in the practice of our profession as embodied in our Code of Ethics</p>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>


</html>
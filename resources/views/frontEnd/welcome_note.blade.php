@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

     @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="welcome_note_ijirk_text fix">
                <h2 style="text-align: center;margin-top: 10px;">WELCOME TO IJCRS!</h2><br/>

                <p>International Journal of Creative Research and Studies (IJCRS) is a high quality open-access, peer-reviewed and refereed multidisciplinary research journal, dedicated to serve the society by the global dissemination of information through an unparalleled commitment to quality, reliability, and innovation and research work which is published by the Knowledge Ridge Publishers. International Journal of Creative Research and Studies (IJCRS) is a brisk educational research platform providing those individuals an ideal feeling to accomplish their desires who long for a refined betterment in their respective arenas.<br/><br/>

We are a decisive, fast-moving company open to new ideas and creative publishing. We are also committed to nurturing long-term relationships with our authors and supporting them throughout their careers. We acquire, develop, market, and distribute knowledge by disseminating scholarly and professional materials throughout the world. All of the journals published by us maintain the highest standards of quality, with Editorial Boards composed of scholars from around the world.<br/><br/>

We seek to acquaint a wide spectrum of readers with the quality research being done in various educational institutions, research bodies and intellectual firms. Therefore, we welcome wide comparative and transnational studies, essays, research papers that are addressing this community’s qualitative and quantitative concerns. Importance and preference will be given to those articles that address and contribute to important disciplinary and interdisciplinary queries, clarifications, problem statements and controversies. We engage our noble efforts for the development. We have tried our best to give you better. IJCRS is much caring and faster way to publish your article without hesitation.<br/><br/>

IJCRS aims to promote academic interchange and attempts to sustain a closer cooperation among academics, researchers, policy makers and practitioners from a wide range of disciplines.<br/>
<br/>
IJCRS welcomes and acknowledges high quality theoretical and empirical original research papers, case studies, review papers, literature reviews, book reviews, conceptual framework, analytical and simulation models, technical notes from researchers, academicians, professional, practitioners and students from all over the world. The journal will continue to publish high quality papers and will also ensure that the published papers achieve broad international credibility. The journal publishes original research articles on a wide range of topics of contemporary relevance in the broad fields of Business, Economics and Management, Accounting and Finance, Social Science, Literature, Arts and Humanities, Engineering & Technology, Life Science and Physical Science, Health and Medical Science, and related issues.

                </p>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>
</html>
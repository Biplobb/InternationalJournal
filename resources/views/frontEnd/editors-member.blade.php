@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')

        <div class="fix content_area">
            <div class="ijir_text fix">
                <h1 style="text-align: center;margin-top: 10px;"> EDITORIAL BOARD</h1><br/>
                <h3 style="text-align: center;margin-top: 10px;"> EDITOR-IN-CHIEF</h3>
                <h3 style="text-align: center;margin-top: 10px;"> Dr. Michael Hall</h3><br/>


                <br/><br/>
                <h3>Editorial Members</h3>
                <table class="editor_board_table">
                    <tr>
                        <th>
                            <p>Members</p>
                        </th>

                    </tr>

                    @foreach($paper_info as $paper)
                    <tr>
                        <td>
                            <p style="margin-left: 10px;"><b>Name:</b> {{$paper->name}} </p>
                            <p style="margin-left: 10px;"><b>Details:</b> {{$paper->department}}</p>

                        </td>

                    </tr>

                        @endforeach


                </table>

            </div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>
</body>

</html>
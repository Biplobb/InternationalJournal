@include('frontEnd.layouts.head')

<div class="fix main">

    @include('frontEnd.layouts.header-top')

    <div class="fix maincontent">

        @include('frontEnd.layouts.sidebar')
        <div class="fix content_area">
            <div class="ijir_text fix">
                <h1>Archive</h1><br>
                <div class="volume_header">



                    @foreach($archives as $archive)

                    <div class=""><br>
                        <button type="button" id="getFiles" data-id="{{$archive->month}}">{{$archive->volumes}}</button><br>
                    </div>

                    @endforeach
                </div><br>
                <div class="separator_volume"></div><br>

            </div>

            <div id="data"></div>
        </div>
        @include('frontEnd.layouts.down-footer')
    </div>
</div>

<script>

    $(document).on('click','#getFiles',function () {

        $('#data').empty();
        $.ajax({
            type:'post',
            url:'{{route('getData')}}',
            data:{
                _token:'{{csrf_token()}}',
                month:$(this).data('id')
            },
            success:function (data) {
                $('#data').append(data);
            }

        })

    })
</script>

</body>

</html>
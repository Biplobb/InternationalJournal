<?php

Route::get('/welcome_note', function () {
    return view('frontEnd.welcome_note');
});

Route::get('/aim_scope', function () {
    return view('frontEnd.aim_scope');
});

Route::get('/mission_vision', function () {
    return view('frontEnd.mission_vision');
});

Route::get('/policies', function () {
    return view('frontEnd.policies');
});

Route::get('/ethical', function () {
    return view('frontEnd.ethical');
});

Route::get('/publisher', function () {
    return view('frontEnd.publisher');
});

Route::get('/author', function () {
    return view('frontEnd.author');
});

Route::get('/duties-author', function () {
    return view('frontEnd.duties-author');
});

Route::get('/guideline-author', function () {
    return view('frontEnd.guideline-author');
});

Route::get('/duties-of-editor', function () {
    return view('frontEnd.duties-of-editor');
});



Route::get('/journal-index', function () {
    return view('frontEnd.journal-index');
});

Route::get('/why-us', function () {
    return view('frontEnd.why-us');
});

Route::get('/faq', function () {
    return view('frontEnd.faq');
});

Route::get('/paper-submission', function () {
    return view('frontEnd.paper-submission');
});

Route::get('/review-process', function () {
    return view('frontEnd.review-process');
});

Route::get('/publication-fee', function () {
    return view('frontEnd.publication-fee');
});

Route::get('/editorial', function () {
    return view('frontEnd.editorial');
});





/*Paper Submission*/


Route::get('/','PaperSubmitController@frontEndIndex');
Route::get('/.env', 'PaperSubmitController@frontEndIndex');
Route::get('/.env.example', 'PaperSubmitController@frontEndIndex');
Route::get('/.gitattributes', 'PaperSubmitController@frontEndIndex');
Route::get('/.gitignore', 'PaperSubmitController@frontEndIndex');
Route::get('/artisan', 'PaperSubmitController@frontEndIndex');
Route::get('/server.php', 'PaperSubmitController@frontEndIndex');

Route::get('/add-paper','PaperSubmitController@index');

Route::post('/save-paper','PaperSubmitController@store');

Route::get('/manage-paper','PaperSubmitController@manage');

Route::get('/edit-paper/{id}','PaperSubmitController@edit');

Route::post('/update-paper','PaperSubmitController@update');

Route::get('/delete-paper/{id}','PaperSubmitController@delete');

/*Paper Submission date-1*/


Route::get('/submit-paper','PaperSubmissionDateOne@submitPaper');

Route::get('/add-date-1','PaperSubmissionDateOne@index');

Route::post('/save-date-1','PaperSubmissionDateOne@store');

Route::get('/manage-date-1','PaperSubmissionDateOne@manage');

Route::get('/edit-date-1/{id}','PaperSubmissionDateOne@edit');

Route::post('/update-date','PaperSubmissionDateOne@update');

Route::get('/delete-date/{id}','PaperSubmissionDateOne@delete');

/**Call For Paper*/

Route::get('/call-for-paper','CallPaperController@frontEndindex');

Route::get('/add-call-paper','CallPaperController@index');

Route::post('/save-call-paper','CallPaperController@store');

Route::get('/manage-call-paper','CallPaperController@manage');

Route::get('/edit-call-paper/{id}','CallPaperController@edit');

Route::get('/delete-call-paper/{id}','CallPaperController@delete');

Route::post('/update-call-paper','CallPaperController@update');


/**Editors Memebr**/



Route::get('/editors-member','EditorsMemberController@frontEndindex');

Route::get('/add-editors-member','EditorsMemberController@index');

Route::post('/save-editors-member','EditorsMemberController@store');

Route::get('/manage-editors-member','EditorsMemberController@manage');

Route::get('/edit-editors-member/{id}','EditorsMemberController@edit');

Route::get('/delete-editors-member/{id}','EditorsMemberController@delete');

Route::post('/update-editors-member','EditorsMemberController@update');

/** Notice **/


Route::get('/add-notice','NoticeController@index');

Route::post('/save-notice','NoticeController@store');

Route::get('/manage-notice','NoticeController@manage');

Route::get('/edit-notice/{id}','NoticeController@edit');

Route::get('/delete-notice/{id}','NoticeController@delete');

Route::post('/update-notice','NoticeController@update');

/** Current Issue **/


Route::get('/current-issue','CurrentIssueController@frontEndindex');

Route::get('/add-current-issue','CurrentIssueController@index');

Route::post('/save-current-issue','CurrentIssueController@store');

Route::get('/manage-current-issue','CurrentIssueController@manage');

Route::get('/edit-current-issue/{id}','CurrentIssueController@edit');

Route::get('/delete-current-issue/{id}','CurrentIssueController@delete');

Route::post('/update-current-issue','CurrentIssueController@update');

Route::get('/issue-details/{id}','CurrentIssueController@issueDetails');
Route::get('/issue-details-single/{id}','CurrentIssueController@issueDetailsSingle');

/** Current Issue  Date**/

Route::get('/add-current-issue-date','CurrentIssueController@currentIndex');

Route::post('/save-current-issue-date','CurrentIssueController@currentStore');

Route::get('/manage-current-issue-date','CurrentIssueController@currentManage');

Route::get('/edit-current-issue-date/{id}','CurrentIssueController@currentEdit');

Route::get('/delete-current-issue-date/{id}','CurrentIssueController@currentDelete');

Route::post('/update-current-issue-date','CurrentIssueController@currentUpdate');

/** Archive  Issue**/

Route::get('/archive','archiveController@index');

/*pdf download*/

Route::get('/pdf/{id}','CurrentIssueController@pdf');

/*Contact Info*/

Route::get('/contact','ContactController@index');

Route::post('/save-contact','ContactController@store');

Route::get('/manage-contact','ContactController@manage');

Route::get('/delete-contact/{id}','ContactController@delete');

/** Admin Login **/

Route::get('/admin','AdminController@AdminIndex');

Route::get('/logout','AdminController@logout');

Route::get('/admin-panel','AdminController@index');

Route::post('/admin-login-check','AdminController@auth_check');
/** search and pdf */
Route::post('searchPDF','SearchPdfController@search')->name('searchPDF');
Route::get('/pdf/{id}','PdfController@getpdf');

Route::post('getData','archiveController@getData')->name('getData');
Route::get('getfile/{id}','archiveController@getfile');








